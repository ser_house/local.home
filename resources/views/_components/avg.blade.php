<?php
/**
 * @var $items \App\Domain\Common\UI\LabelValueView[]
 */
?>
<fieldset>
  <legend>{{$title}}</legend>
  <table class="avg-items">
    <thead>
    <tr>
      <th class="label">{{$label_title}}</th>
      <th class="value">{{$value_title}}</th>
    </tr>
    </thead>
    @foreach($items as $item)
      <tr>
        <td class="label">{{ $item->label }}</td>
        <td class="value">{{ $item->value }}</td>
      </tr>
    @endforeach
  </table>
</fieldset>
