<?php
/**
 * @var $last \App\Domain\Ambient\UI\MeasureView[] | null
 * @var $ws_host string
 * @var $ws_port string
 * @var $last_hour_items \App\Domain\Ambient\UI\MeasureView[]
 * @var $last_day_items \App\Domain\Ambient\UI\MeasureView[]
 * @var $today_missing_count int
 */
?>
@extends('app')
@section('page_title', 'Главная')

@push('styles')
  <link href="{{ asset('css/ambient.css') }}" rel="stylesheet">
@endpush
@push('scripts')
  <script>
    var ws_host = @json($ws_host);
    var ws_port = @json($ws_port);
    var token = @json($token);
    var env = @json(Illuminate\Support\Facades\App::environment());
  </script>
  <script src="{{ mix('js/pages/ambient/home.js') }}"></script>
  <script src="{{ mix('js/utils/tabs.js') }}"></script>
@endpush
@section('content_class', 'dashboard')
@section('content')
  <ul class="sub-menu">
    <li><a href="{{ route('ambient.missing_minutes_report') }}">Пропущенные минуты</a></li>
  </ul>
  @if($today_missing_count)
    <div class="alert alert-warning">
      Пропущенных минут за сегодня: {!! $today_missing_count !!}
    </div>
  @endif
  <fieldset class="ambient">
    <legend>Температура и влажность</legend>
    @if($last)
      <div class="last-measure">
        <div class="last-measure-content">
          <div class="house">
            <label>Комната</label>
            <div class="last-measure-values">
              <div class="temperature"><span id="last-measure-house-temperature">{{$last[\App\Domain\Ambient\Sensor::ROOM1]->t}}</span>&deg;C</div>
              <div class="humidity"><span id="last-measure-house-humidity">{{$last[\App\Domain\Ambient\Sensor::ROOM1]->h}}</span>%</div>
            </div>
          </div>
          <div class="outdoors">
            <label>Улица</label>
            <div class="last-measure-values">
              <div class="temperature"><span id="last-measure-outdoors-temperature">{{$last[\App\Domain\Ambient\Sensor::OUTDOORS1]->t}}</span>&deg;C</div>
            </div>
          </div>
        </div>
        <div class="label">
          <div>Обновлено</div>
          <div>(<span id="last-measure-time">{{$last[\App\Domain\Ambient\Sensor::OUTDOORS1]->time->ui}}</span>)</div>
        </div>
      </div>
    @endif
    <div class="tabs">
      <ul class="tabs-links">
        <li class="active">
          <button type="button" class="btn btn-link link" data-content="by-hours-chart">За сутки</button>
        </li>
        <li>
          <button type="button" class="btn btn-link link" data-content="last-hour-chart">За час</button>
        </li>
      </ul>
      <div class="tab-content">
        <div class="content by-hours-chart active">
          <ambient-chart id="by-hours-chart" :data='@json($last_day_items)' add-data-event="last_day_update"></ambient-chart>
        </div>
        <div class="content last-hour-chart">
          <ambient-chart id="last-hour-chart" :data='@json($last_hour_items)' add-data-event="last_hour_update"></ambient-chart>
        </div>
      </div>
    </div>

  </fieldset>
@endsection
