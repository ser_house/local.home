<?php
/**
 * @var $items \App\Domain\Ambient\MissingMinutes\UI\DateView[]
 */
?>
@extends('app')
@section('page_title', 'Пропущенные минуты')

@push('styles')
  <link href="{{ asset('css/ambient.css') }}" rel="stylesheet">
@endpush

@push('scripts')
  <script src="{{ mix('js/pages/ambient/missing_minutes_report.js') }}"></script>
@endpush

@section('content')
  <h1>Пропущенные минуты</h1>
  @if($items)
    <missing-minutes :missing-minutes='@json($items)'/>
  @else
    Нет пропущенных минут.
  @endif
@endsection
