<?php
/**
 * @var $date string
 * @var $value string
 */
?>
<form class="weight-add-form inline-form" action="{{route('weight.add')}}" method="POST">
  @csrf
  <div class="form-group form-inline-group">
    <input class="date-input" name="date" type="date" value="{!! old('date', $date) !!}">
    <input class="value-input" name="value" type="number" step="0.1" value="{!! old('value', $value) !!}">
    <button type="submit" class="btn btn-primary">добавить</button>
  </div>
</form>
