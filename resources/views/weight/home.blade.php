<?php
/**
 * @var $new_is_allowed bool
 * @var $date string|null
 * @var $value string
 * @var $last_items \App\Domain\Common\UI\LabelValueView[]
 * @var $avg_items \App\Domain\Common\UI\LabelValueView[]
 */
$last_year_options = [
  'title' => 'Последний год',
  'line_color' => 'blue',
];
$by_years_options = [
  'title' => 'В среднем по годам',
  'line_color' => 'green',
];
?>
@extends('app')
@section('page_title', 'Вес')

@push('styles')
  <link href="{{ mix('css/chart.css') }}" rel="stylesheet">
@endpush

@push('scripts')
  <script src="{{ mix('js/chart/chart.js') }}"></script>
@endpush

@section('content')
  @if($last_items)
    <div class="line-chart" data-items='@json($last_items)' data-options='@json($last_year_options)'></div>
  @endif
  @if($new_is_allowed)
    @component('weight.form', ['date' => $date, 'value' => $value])@endcomponent
  @endif
  @if($avg_items)
    <div class="line-chart" data-items='@json($avg_items)' data-options='@json($by_years_options)'></div>
  @endif
@endsection
