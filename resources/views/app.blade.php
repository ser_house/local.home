<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('page_title')</title>

  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  @stack('styles')
</head>
<body>
<section id="app" class="container">
  @component('main-menu')@endcomponent
  <section class="content @yield('content_class', '')">
    @component('flash')@endcomponent
    @yield('content')
  </section>
</section>
@routes
@stack('scripts')
</body>
</html>
