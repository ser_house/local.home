<?php
/**
 * @var $new_is_allowed bool
 * @var $date string|null
 * @var $time string|null
 * @var $last_items \App\Domain\Oga\UI\DateView[]
 * @var $avg_items \App\Domain\Common\UI\LabelValueView[]
 */
$by_years_options = [
  'title' => 'В среднем по годам',
  'line_color' => 'green',
];
?>
@extends('app')
@section('page_title', 'Ога')

@push('styles')
  <link href="{{ mix('css/chart.css') }}" rel="stylesheet">
  <link href="{{ mix('css/oga.css') }}" rel="stylesheet">
@endpush

@push('scripts')
  <script src="{{ mix('js/chart/chart.js') }}"></script>
@endpush

@section('content')
  @component('oga.menu')@endcomponent
  @if($last_items)
    <fieldset>
      <legend>Последние данные</legend>
      <ul class="list-items week oga-last-items last-items">
        @foreach($last_items as $lastItem)
          <li class="{!! $lastItem->css_class !!}">
            <div class="item-info">
              <div class="day-item">
                <div class="weekday">{{ $lastItem->weekday }}</div>
                <div class="date">{{ $lastItem->date }}</div>
              </div>
              <div class="days">{{ $lastItem->days }}</div>
            </div>
          </li>
        @endforeach
      </ul>
    </fieldset>
  @endif
  <div class="flexed">
    @if($new_is_allowed)
      @component('oga.form', ['date' => $date, 'time' => $time])@endcomponent
    @endif
    <a href="{{route('oga.import')}}">импортировать</a>
  </div>
  @if($avg_items)
    <div class="line-chart" data-items='@json($avg_items)' data-options='@json($by_years_options)'></div>
  @endif
@endsection
