<?php
/**
 * @var $content string
 * @var $times array[]
 */
?>
@extends('app')
@section('page_title', 'Импорт')

@section('content')
  <form class="oga-import-form" action="{{route('oga.import')}}" method="POST">
    @csrf
    <textarea name="content" rows="7">{!! old('content', $content) !!}</textarea>
    @if($times)
      <ul class="list-items week">
        Предпросмотр
        @foreach($times as $item)
          <li>
            <div class="form-group form-inline-group">
              <div>{!! $item['row'] !!}</div>
              <input class="date-input" name="date[{!! $loop->index !!}]" type="date" value="{!! old("date[$loop->index]", $item['date']) !!}">
              <input class="time-input" name="time[{!! $loop->index !!}]" type="time" step="300" value="{!! old("time[$loop->index]", $item['time']) !!}">
            </div>
          </li>
        @endforeach
      </ul>
    @endif
    <div class="actions">
      <button type="submit" class="btn btn-primary">импортировать</button>
    </div>
  </form>
@endsection
