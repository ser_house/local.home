<?php
/**
 * @var $date string
 * @var $time string
 */
?>
<form class="oga-add-form inline-form" action="{{route('oga.add')}}" method="POST">
  @csrf
  <div class="form-group form-inline-group">
    <input class="date-input" name="date" type="date" value="{!! old('date', $date) !!}">
    <input class="time-input" name="time" type="time" step="300" value="{!! old('time', $time) !!}">
    <button type="submit" class="btn btn-primary">добавить</button>
  </div>
</form>
