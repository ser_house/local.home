<?php
/**
 * @var $data \App\Framework\Oga\Month[]
 */

?>
@extends('app')
@section('page_title', 'Календарь')

@push('styles')
  <link href="{{ mix('css/calendar.css') }}" rel="stylesheet">
@endpush

@section('content')
  @component('oga.menu')@endcomponent
  <div class="calendar">
    @foreach($data as $month)
      <table class="month">
        <thead>
        <tr>
          <td colspan="7" class="month-name">{!! $month->getName() !!}</td>
        </tr>
        <tr>
          @foreach($month->getWeekdayNames() as $weekdayName)
            <td class="weekday-name">{!! $weekdayName !!}</td>
          @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($month->getWeeks() as $week)
          <tr class="week">
            @foreach($week as $day)
              @if($day)
                <td class="day{!! $day->cssClasses() !!}" @if($day->time)title="{!! $day->time !!}"@endif>{!! $day->day !!}</td>
              @else
                <td class="day-empty"></td>
              @endif
            @endforeach
          </tr>
        @endforeach
        </tbody>
      </table>
    @endforeach
  </div>
@endsection
