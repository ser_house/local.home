@spaceless
<ul class="sub-menu">
  <li><a class="menu-item @if('main' === $active)active @endif" href="{{ route('main') }}">Текущее</a></li>
  <li><a class="menu-item @if('oga.calendar' === $active)active @endif" href="{{ route('oga.calendar') }}">Календарь</a></li>
</ul>
@endspaceless
