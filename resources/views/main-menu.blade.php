<nav class="main-menu-bar">
  <a class="home-link @if('main' === $active)active @endif" href="{{ route('main') }}">{{env('APP_NAME', '')}}</a>
{{--  <a @if('plants' === $active)class="active" @endif href="{{ route('plants') }}">Растения</a>--}}
{{--  <a @if('oga' === $active)class="active" @endif href="{{ route('oga') }}">Ога</a>--}}
  <a @if('weight' === $active)class="active" @endif href="{{ route('weight') }}">Вес</a>
</nav>
