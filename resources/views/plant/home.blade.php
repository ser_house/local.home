<?php
/**
 * @var $plants array
 */
?>
@extends('app')
@section('page_title', 'Растения')

@push('styles')
  <link href="{{ asset('css/plants.css') }}" rel="stylesheet">
@endpush

@push('scripts')
  <script src="{{ mix('js/pages/plant/home.js') }}"></script>
@endpush

@section('content')
  <ul class="sub-menu">
    <li><a href="{{ route('plants.new') }}">Добавить</a></li>
  </ul>

  @if(count($plants))
    <div class="plants">
      @foreach($plants as $plant)
        <home-item :init-plant='@json($plant)'></home-item>
      @endforeach
    </div>
  @endif
@endsection
