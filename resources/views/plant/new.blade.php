<?php
/**
 * @var $data array
 */
?>
@extends('app')
@section('page_title', 'Новое растение')

@push('styles')
  <link href="{{ asset('css/plants.css') }}" rel="stylesheet">
@endpush

@push('scripts')
  <script src="{{ mix('js/pages/plant/new.js') }}"></script>
@endpush

@section('content')
  <h2>Новое растение</h2>
  <form method="post" enctype="multipart/form-data" action="{{route('plants.add')}}" class="new-plant-form">
    @csrf
    <div class="form-group">
      <label for="title">Название</label>
      <input id="title" name="title" type="text" value="{{ old('title', $data['title']) }}" required/>
    </div>
    <div class="form-group checkbox">
      <label for="active"><input id="active" name="active" type="checkbox" @if(old('active', $data['active'])) checked @endif/>активен</label>
    </div>
    <div class="form-group">
      <label for="description">Описание</label>
      <textarea id="description" name="description">{{ old('description', $data['description']) }}</textarea>
    </div>
    <periodicity :periodicity='@json($data['periodicity'])' :summer-periodicity="{!! old('summer_periodicity', $data['summer_periodicity']) !!}" :winter-periodicity="{!! old('summer_periodicity', $data['summer_periodicity']) !!}"
      :today-is-summer='@json($data['today_is_summer'])'></periodicity>
    <div class="actions">
      <button type="submit" class="btn btn-primary">Добавить</button>
    </div>
  </form>
@endsection
