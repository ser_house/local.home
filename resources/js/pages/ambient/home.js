import {Sensor} from "../../Sensor";

import {createApp} from 'vue'
import AmbientChart from "../../vue/ambient/AmbientChart";
let app = createApp({});

import EventBus from "../../vue/EventBus";
app.config.globalProperties.eventBus = EventBus;

app.component('AmbientChart', AmbientChart).mount('#app');

import Centrifuge from "centrifuge";
let centrifuge = new Centrifuge(`ws://${ws_host}:${ws_port}/connection/websocket`);
centrifuge.setToken(token);
let measureBySid = (data, sid) => data.filter(item => item.sid === sid);

centrifuge.subscribe(`last_ambient-${env}`, function (message) {
  EventBus.trigger('new_last_ambient', message.data);

  let indoors = measureBySid(message.data.data, Sensor.ROOM1)[0];
  let outdoors = measureBySid(message.data.data, Sensor.OUTDOORS1)[0];
  document.getElementById("last-measure-time").innerHTML = indoors.time.ui;
  document.getElementById("last-measure-house-temperature").innerHTML = indoors.t;
  document.getElementById("last-measure-house-humidity").innerHTML = indoors.h;
  document.getElementById("last-measure-outdoors-temperature").innerHTML = outdoors.t;
});

centrifuge.subscribe(`last_day_update-${env}`, function (message) {
  EventBus.trigger('last_day_update', message.data);
});
centrifuge.subscribe(`last_hour_update-${env}`, function (message) {
  EventBus.trigger('last_hour_update', message.data);
});

centrifuge.connect();
