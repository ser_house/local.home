import {createApp} from 'vue'
import MissingMinutes from "../../vue/ambient/MissingMinutes";
let app = createApp({});

app.component('MissingMinutes', MissingMinutes).mount('#app');
