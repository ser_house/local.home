window.addEventListener('DOMContentLoaded', function() {

  document.querySelectorAll('.tabs .link').forEach(function(el) {
    el.addEventListener('click', (event) => {
      Array.from(el.closest('.tabs').getElementsByClassName('active')).forEach((item) => item.classList.remove('active'));
      let content = el.closest('.tabs').getElementsByClassName(el.dataset.content)[0];
      content.classList.add('active');
      el.parentElement.classList.add('active');
    });
  });
});
