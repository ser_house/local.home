const CIRCLE = Math.PI * 2;

const WHITE_FILL_STYLE = 'rgba(255, 255, 255, 1)';

import {get_str_max_len, get_str_size} from './strings';

export class Values {

  constructor(items, font_size) {
    this.items = items;
    this.font_size = font_size;

    this.left = 0;
    this.bottom = 0;

    let values = this.items.map(({value}) => value);
    let max_value_str = get_str_max_len(values);
    let size = get_str_size(max_value_str, this.font_size);
    this.value_w = size.w;
    this.value_h = size.h;

    this.line_color = 'blue';
  }

  update(left, bottom) {
    this.left = left;
    this.bottom = bottom;
  }

  draw(ctx, axiosX, axiosY) {
    ctx.save();

    let thickness = 2;

    ctx.lineWidth = thickness;

    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.font = `${this.font_size}px Verdana`;
    ctx.strokeStyle = this.line_color;

    let x = this.left;
    let val_in_px = axiosY.val_in_px;

    for (let i = 0; i < this.items.length; i++) {
      let value_str = this.items[i].value;
      let value = parseFloat(this.items[i].value.replace(',', '.'));
      let value_px = (value - axiosY.min) * 10 / val_in_px;

      if ('undefined' !== typeof this.items[i + 1]) {
        let next_value = parseFloat(this.items[i + 1].value.replace(',', '.'));
        let next_value_px = (next_value - axiosY.min) * 10 / val_in_px;
        let y = this.bottom - value_px;
        let end_y = this.bottom - next_value_px;
        let end_x = x + axiosX.step;

        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(end_x, end_y);
        ctx.stroke();

        this.drawValueDot(ctx, x, y);
        this.drawValue(ctx, x, y, value_str);
        this.drawProjectionLine(ctx, end_x, end_y);
      }
      else {
        let end_y = this.bottom - value_px;

        this.drawValueDot(ctx, x, end_y);
        this.drawValue(ctx, x, end_y, value_str);
      }

      x += axiosX.step;
    }
    ctx.restore();
  }

  drawValue(ctx, x, y, value_str) {
    let text_y = y - this.font_size * 1.5;

    ctx.save();

    // ctx.beginPath();
    // ctx.fillStyle = WHITE_FILL_STYLE;
    // let w = this.value_w * 1.5;
    // let bg_x = x - w / 2;
    // let bg_y = text_y - this.value_h / 2;
    // ctx.rect(bg_x, bg_y, w, this.value_h);
    // ctx.fill();
    // ctx.lineWidth = 1;
    // ctx.strokeStyle = 'black';
    // ctx.stroke();
    // ctx.restore();

    ctx.beginPath();
    ctx.fillText(value_str, x, text_y);
    ctx.restore();
  }

  drawProjectionLine(ctx, start_x, start_y) {
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'gray';
    ctx.setLineDash([10, 10]);
    ctx.moveTo(start_x, start_y);
    ctx.lineTo(start_x, this.bottom - 5);
    ctx.stroke();
    ctx.restore();
  }

  drawValueDot(ctx, x, y) {
    ctx.save();

    ctx.beginPath();
    ctx.arc(x, y, 3, 0, CIRCLE);
    ctx.fillStyle = WHITE_FILL_STYLE;
    ctx.fill();
    ctx.lineWidth = 2;
    // ctx.strokeStyle = 'black';
    ctx.stroke();

    ctx.restore();
  }
}
