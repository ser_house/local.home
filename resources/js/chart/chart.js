const BASE_PADDING = 40;

import {AxiosY} from './AxiosY';
import {AxiosX} from './AxiosX';
import {Values} from './Values';

class LineChart {
  constructor(el) {


    this.items = JSON.parse(el.getAttribute('data-items'));
    let options = JSON.parse(el.getAttribute('data-options'));

    this.options = {
      title: options.title || '',
      line_color: options.line_color || 'black',
      bg_color: options.bg_color || 'rgba(255, 255, 255, 1)',
      font_size: options.font_size || 14,
      width: options.width || '100%',
      height: options.height || '400px',
    };

    if (this.options.title.length) {
      let title_el = document.createElement('h3');
      title_el.textContent = this.options.title;
      el.appendChild(title_el);
    }


    this.canvas = document.createElement('canvas');
    this.canvas.id = 'canvas';
    this.canvas.classList.add('canvas');
    this.canvas.innerText = 'Your browser does not support JS or HTML5!';
    el.appendChild(this.canvas);

    this.ctx = this.canvas.getContext('2d');

    this.axiosY = new AxiosY(this.items, this.options.font_size);
    this.axiosX = new AxiosX(this.items, this.options.font_size);

    this.values = new Values(this.items, this.options.font_size);
    this.values.line_color = this.options.line_color;

    this.left_padding = this.axiosY.text_w + BASE_PADDING;
    this.top_padding = BASE_PADDING;
    this.bottom_padding = this.axiosX.text_45_height + BASE_PADDING;
    this.right_padding = BASE_PADDING;


    let resize = () => {
      this.canvas.style.width = this.options.width;
      this.canvas.style.height = this.options.height;

      this.canvas.width = this.canvas.clientWidth * window.devicePixelRatio;
      this.canvas.height = this.canvas.clientHeight * window.devicePixelRatio;

      this.draw();
    };
    window.addEventListener('resize', resize, false);

    resize();
  }

  _initTopBottom() {
    this.left = this.left_padding;
    this.top = this.top_padding;
    this.right = this.canvas.width - this.right_padding;
    this.bottom = this.canvas.height - this.bottom_padding;

    this.axiosY.update(this.left, this.top, this.bottom);
    this.axiosX.update(this.left, this.right, this.bottom);
    this.values.update(this.left, this.bottom);
  }

  draw() {
    this._initTopBottom();

    this.ctx.save();
    this.ctx.fillStyle = this.options.bg_color;
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.restore();

    this.axiosY.draw(this.ctx);
    this.axiosX.draw(this.ctx);
    this.values.draw(this.ctx, this.axiosX, this.axiosY);
  }
}

window.addEventListener('DOMContentLoaded', function () {
  let elements = document.querySelectorAll('.line-chart');
  elements.forEach(function (el) {
    new LineChart(el);
  });
});
