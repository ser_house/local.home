const RAD_45 = 45 * Math.PI / 180;
const SIN_X_45 = Math.sin(RAD_45);
const COS_X_45 = Math.cos(RAD_45);

export class TextRotator {

  constructor(text_width) {
    this.width_45 = text_width * SIN_X_45;
    this.height_45 = text_width * COS_X_45;

    this.counterclockwise_45 = -1 * RAD_45;
    this.clockwise_45 = RAD_45;
  }
}
