import {get_str_max_len, get_str_size} from './strings';


export class AxiosY {
  constructor(items, font_size) {
    this.items = items;
    this.font_size = font_size;

    this.left = 0;
    this.top = 0;
    this.bottom = 0;

    this.height = 0;
    this.step = 0;

    this.min = 0;
    this.max = 0;
    this.range = 0;
    this.val_in_px = 0;

    this.labels = this._getLabelsY(this.items);

    let max_str = get_str_max_len(this.labels);
    let size = get_str_size(max_str, this.font_size);
    this.text_w = size.w;
    this.text_h = size.h;

    // @todo: isFloat для значений, и сколько знаков после запятой (до скольки делать toFixed()).
  }

  update(left, top, bottom) {
    this.left = left;
    this.top = top;
    this.bottom = bottom;

    this.height = this.bottom - this.top;
    this.val_in_px = this.range * 10 / this.height;

    this.step = this.height / (this.labels.length - 1);
  }

  draw(ctx) {
    ctx.save();

    ctx.lineWidth = 1;

    ctx.beginPath();
    ctx.moveTo(this.left, this.top);
    ctx.lineTo(this.left, this.bottom);

    ctx.stroke();

    ctx.fillStyle = 'black';
    ctx.font = `${this.font_size}px Verdana`;

    ctx.textAlign = 'end';
    ctx.textBaseline = 'middle';

    let y = this.bottom;
    // Для middle сдвигаем наполовину ширины.
    let text_x = this.left - this.text_w / 2;
    for (let i in this.labels) {
      ctx.fillText(this.labels[i], text_x, y);
      ctx.beginPath();
      ctx.moveTo(this.left - 5, y);
      ctx.lineTo(this.left + 5, y);
      ctx.stroke();
      y -= this.step;
    }

    ctx.restore();
  }

  _getLabelsY(items) {
    let values = [];
    items.forEach((item) => {
      let value = parseFloat(item.value.replace(',', '.'));
      values.push(value);
    });

    let max = Math.max(...values);
    // Округление до десятка.
    // this.max = Math.ceil(max / 10) * 10;
    this.max = Math.ceil(max);
    if (0 !== this.max % 2) {
      this.max += 1;
    }
    let min = Math.min(...values);
    // Округление до десятка.
    // this.min = Math.floor(min / 10) * 10;

    this.min = Math.floor(min);
    if (0 !== this.min % 2) {
      this.min -= 1;
    }

    this.range = this.max - this.min;

    let step = Math.ceil(this.range / 10);
    values = [];
    for (let i = this.min; i <= this.max; i += step) {
      let value = i.toFixed(1);
      values.push(value.toString());
    }

    return values;
  }
}
