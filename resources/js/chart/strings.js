import {Size} from './Size';

export function get_str_max_len(strings) {
  let len = 0;
  let max_str = '';
  for (let str of strings) {
    if (str.length > len) {
      len = str.length;
      max_str = str;
    }
  }
  return max_str;
}

export function get_str_size(str, font_size) {
  let elemDiv = document.createElement('div');
  elemDiv.innerHTML = str;
  elemDiv.style.fontSize = font_size;
  elemDiv.style.position = 'absolute';
  document.body.appendChild(elemDiv);

  let height = (elemDiv.clientHeight + 1);
  let width = (elemDiv.clientWidth + 1);
  elemDiv.remove();

  return new Size(width, height);
}
