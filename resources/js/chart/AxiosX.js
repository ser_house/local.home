import {get_str_max_len, get_str_size} from './strings';
import {TextRotator} from './TextRotator';

export class AxiosX {
  constructor(items, font_size) {
    this.items = items;
    this.font_size = font_size;

    this.left = 0;
    this.right = 0;
    this.bottom = 0;

    this.width = 0;
    this.step = 0;

    this.labels = this.items.map(({label}) => label);

    let max_str = get_str_max_len(this.labels);
    let size = get_str_size(max_str, this.font_size);
    this.text_w = size.w;
    this.text_h = size.h;

    let textRotator = new TextRotator(this.text_w);
    this.text_45_width = textRotator.width_45;
    this.text_45_height = textRotator.height_45;
    this.rotate_angle = textRotator.counterclockwise_45;
  }

  update(left, right, bottom) {
    this.left = left;
    this.right = right;
    this.bottom = bottom;

    this.width = this.right - this.left;
    this.step = this.width / (this.labels.length - 1);
  }

  draw(ctx) {
    ctx.save();

    ctx.lineWidth = 1;

    ctx.beginPath();
    ctx.moveTo(this.left, this.bottom);
    ctx.lineTo(this.right, this.bottom);

    ctx.stroke();

    ctx.fillStyle = 'black';
    ctx.font = `${this.font_size}px Verdana`;

    ctx.textAlign = 'left';
    ctx.textBaseline = 'top';

    let text_y = this.bottom + this.text_45_height + 10;
    let x = this.left;

    for (let i = 0; i < this.labels.length; i++) {
      ctx.save();
      ctx.translate(x - this.text_45_width, text_y);
      ctx.rotate(this.rotate_angle);
      ctx.fillText(this.labels[i], 0, 0);
      ctx.restore();

      ctx.beginPath();
      ctx.moveTo(x, this.bottom - 5);
      ctx.lineTo(x, this.bottom + 5);
      ctx.stroke();
      x += this.step;
    }

    ctx.restore();
  }
}
