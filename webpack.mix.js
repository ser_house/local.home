const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js');

mix.js('resources/js/utils/tabs.js', 'public/js/utils');

mix.js('resources/js/pages/ambient/home.js', 'public/js/pages/ambient');
mix.js('resources/js/pages/ambient/missing_minutes_report.js', 'public/js/pages/ambient');

mix.js('resources/js/pages/plant/new.js', 'public/js/pages/plant');
mix.js('resources/js/pages/plant/home.js', 'public/js/pages/plant');

mix.js('resources/js/chart/chart.js', 'public/js/chart');

mix.vue();

mix.sass('resources/sass/app.scss', 'public/css');
mix.sass('resources/sass/ambient.scss', 'public/css');
mix.sass('resources/sass/plants.scss', 'public/css');
mix.sass('resources/sass/oga.scss', 'public/css');
mix.sass('resources/sass/calendar.scss', 'public/css');
mix.sass('resources/sass/chart.scss', 'public/css');

mix.disableNotifications();
mix.sourceMaps(false);

mix.version();
