<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.10.2019
 * Time: 16:48
 */


namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;

class Timezone {

  public function handle(Request $request, Closure $next) {
    $current_tz = Config::get('app.timezone');

    date_default_timezone_set($current_tz);

    return $next($request);
  }
}
