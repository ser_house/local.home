<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Api\Plants;


use App\Domain\Plants\PlantWateringSchedule;
use App\Http\Controllers\Controller;
use App\Models\Plant;
use Illuminate\Http\JsonResponse;


/**
 * Class Watered
 *
 * @package App\Http\Controllers\Api\Plants
 */
class Watered extends Controller {
  private PlantWateringSchedule $plantWateringSchedule;

  /**
   * Watered constructor.
   *
   * @param \App\Domain\Plants\PlantWateringSchedule $plantWateringSchedule
   */
  public function __construct(PlantWateringSchedule $plantWateringSchedule) {
    $this->plantWateringSchedule = $plantWateringSchedule;
  }

  /**
   * @param string $id
   * @param string $date
   *
   * @return JsonResponse
   */
  public function __invoke(string $id, string $date): JsonResponse {
    $plant = Plant::find($id);

    // В базе хранится время по серверу, поэтому в свойствах объекта - время по серверу.
    // @todo: на самом деле получается бардак, потому что разобраться где
    // с какой зоной время становится всё сложнее.
    $wateringDate = new \DateTimeImmutable($date);
    $plant->last_watering_date = $wateringDate->format('Y-m-d');
    $nextWateringDate = $this->plantWateringSchedule->nextWateringDate($wateringDate, $plant->summer_periodicity, $plant->winter_periodicity);

    $plant->next_watering_date = $nextWateringDate->format('Y-m-d');

    if (!empty($plant->save())) {
      $result = [
        'type' => 'success',
      ];
    }
    else {
      $result = [
        'type' => 'fail',
      ];
    }

    return response()->json($result);
  }
}
