<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Api\Ambient;


use App\Domain\Ambient\TemperatureNormalizer;
use App\Domain\DateTimeService;
use App\Http\Controllers\Controller;
use App\Jobs\AddAmbient;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;


/**
 * Class Add
 *
 * @package App\Http\Controllers\Api\Ambient
 */
class Add extends Controller {

  public function __construct(
    private DateTimeService $dateTimeService,
    private TemperatureNormalizer $temperatureNormalizer
  ) {

  }


  /**
   * @param Request $request
   *
   * @return JsonResponse
   * @throws \Exception
   */
  public function __invoke(Request $request): JsonResponse {
    $this->sendToDev($request);
    $now = $this->dateTimeService->getNow();
    Log::channel('ambient')->info('API:Add request', [
      'system_time' => $now->format('d.m.Y H:i:s'),
      'indoors' => $request->input('indoors'),
      'outdoors' => $request->input('outdoors'),
    ]);

    AddAmbient::dispatch($this->temperatureNormalizer, $now, $request->input('outdoors'), $request->input('indoors'));

    $result = [
      'type' => 'success',
    ];

    return response()->json($result, Response::HTTP_CREATED);
  }

  /**
   * Отправляет на дев-версию приложения.
   *
   * @param $input
   */
  private function sendToDev(Request $request) {
    if (!env('SEND_TO_DEV', false)) {
      return;
    }
    $input = $request->input();
    $dt = $this->dateTimeService->getNow();
    $input['time'] = $dt->format('d.m.Y H:i:s');

    $ch = curl_init('http://192.168.0.99:8095/api/t');

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($input));
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_exec($ch);
    curl_close($ch);
  }
}
