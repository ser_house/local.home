<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Api\Ambient\MissingMinutes;


use App\Domain\Ambient\MissingMinutes\UI\Presenter;
use App\Domain\DateTimeService;
use App\Framework\Ambient\MissingMinutesLog;
use App\Framework\Ambient\MissingMinutesService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;


/**
 * Class GetDateMinutes
 *
 * @package App\Http\Controllers\Api\Ambient\MissingMinutes
 */
class GetDateMinutes extends Controller {
  private MissingMinutesService $missingMinutesService;
  private MissingMinutesLog $missingMinutesLog;
  private DateTimeService $dateTimeService;
  private Presenter $presenter;

  /**
   * GetDateMinutes constructor.
   *
   * @param MissingMinutesService $missingMinutesService
   * @param MissingMinutesLog $missingMinutesLog
   * @param DateTimeService $dateTimeService
   * @param Presenter $presenter
   */
  public function __construct(MissingMinutesService $missingMinutesService, MissingMinutesLog $missingMinutesLog, DateTimeService $dateTimeService, Presenter $presenter) {
    $this->missingMinutesService = $missingMinutesService;
    $this->missingMinutesLog = $missingMinutesLog;
    $this->dateTimeService = $dateTimeService;
    $this->presenter = $presenter;
  }


  /**
   * @param string $date
   *
   * @return JsonResponse
   * @throws \Exception
   */
  public function __invoke(string $date): JsonResponse {
    $localDate = $this->dateTimeService->getLocalDateTimeFromLocalStr($date);

    $missingDateTimes = $this->missingMinutesService->getByLocalDate($localDate);
    $minutes = [];
    $utcTz = new \DateTimeZone('UTC');

    foreach ($missingDateTimes as $missingDateTime) {
      $log_data = $this->missingMinutesLog->getLogsByMinute($missingDateTime);

      $utcDateTime = $missingDateTime->setTimezone($utcTz);
      $minutes[] = [
        'local' => $missingDateTime->format('d.m.Y H:i'),
        'utc' => $utcDateTime->format('d.m.Y H:i'),
        'logs' => $this->presenter->presentLogItems(...$log_data),
      ];
    }
    $result = [
      'type' => 'success',
      'minutes' => $minutes,
    ];

    return response()->json($result);
  }
}
