<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Weight;


use App\Framework\Weight\AddService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


/**
 * Class Add
 *
 * @package App\Http\Controllers\Web\Weight
 */
class Add extends Controller {

  private AddService $addService;

  /**
   * Add constructor.
   *
   * @param AddService $addService
   */
  public function __construct(AddService $addService) {
    $this->addService = $addService;
  }

  /**
   * @param Request $request
   *
   * @return \Illuminate\Http\RedirectResponse
   * @throws \Exception
   */
  public function __invoke(Request $request) {
    $date = $request->input('date');
    $value = $request->input('value');

    $this->addService->addByLocalString($date, $value);

    return back()->withInput([
      'date' => $date,
      'value' => $value,
    ])->with('success', "Значение $value добавлено.");
  }
}
