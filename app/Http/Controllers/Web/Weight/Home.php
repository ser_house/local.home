<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Weight;


use App\Domain\DateTimeService;
use App\Domain\Weight\IStorage;
use App\Domain\Weight\NewIsAllowChecker;
use App\Domain\Weight\UI\Presenter;
use App\Http\Controllers\Controller;


/**
 * Class Home
 *
 * @package App\Http\Controllers\Web\Weight
 */
class Home extends Controller {

  private DateTimeService $dateTimeService;
  private IStorage $storage;
  private NewIsAllowChecker $newIsAllowChecker;
  private Presenter $presenter;

  /**
   * Home constructor.
   *
   * @param DateTimeService $dateTimeService
   * @param IStorage $storage
   * @param NewIsAllowChecker $newIsAllowChecker
   * @param Presenter $presenter
   */
  public function __construct(
    DateTimeService $dateTimeService,
    IStorage $storage,
    NewIsAllowChecker $newIsAllowChecker,
    Presenter $presenter) {

    $this->dateTimeService = $dateTimeService;
    $this->storage = $storage;
    $this->newIsAllowChecker = $newIsAllowChecker;
    $this->presenter = $presenter;
  }


  /**
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
   * @throws \Exception
   */
  public function __invoke() {

    $today = $this->dateTimeService->getNowAsLocal();

    $new_is_allowed = $this->newIsAllowChecker->isAllow();

    $last_items = $this->storage->getLastItems(12);

    $avg_items = $this->storage->getAvrValuesByYears();

    return view('weight.home', [
      'new_is_allowed' => $new_is_allowed,
      'date' => $new_is_allowed ? $today->format('Y-m-d') : null,
      'value' => '',
      'last_items' => $this->presenter->presentWeightDateItems(...$last_items),
      'avg_items' => $this->presenter->presentItems(...$avg_items),
    ]);
  }
}
