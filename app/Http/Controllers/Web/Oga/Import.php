<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Oga;


use App\Framework\Oga\ImportService;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Throwable;


class Import extends Controller {
  public function __construct(
    private readonly ImportService $importService,
  ) {
  }


  /**
   * @param Request $request
   *
   * @return Application|Factory|View|RedirectResponse|Redirector
   */
  public function __invoke(Request $request) {
    $all = $request->all();
    $content = $all['content'] ?? null;
    $has_data = !empty($all['date']);

    $times = [];
    if ($content) {
      try {
        [$rows, $items] = $this->importService->parse($content);
        foreach ($items as $i => $timeImmutable) {
          $times[] = [
            'row' => $rows[$i],
            'date' => $timeImmutable->format('Y-m-d'),
            'time' => $timeImmutable->format('H:i'),
          ];
        }

        if ($has_data) {
          $all = $request->all();
          $this->importService->import($all['date'], $all['time']);
          return redirect('oga')->with('success', 'Импортировано записей: ' . count($items));
        }
      }
      catch (Throwable $e) {
        return back()->withInput([
          'content' => $content,
        ])->with('fail', $e->getMessage());
      }
    }
    return view('oga.import', [
      'content' => $content,
      'times' => $times,
    ]);
  }
}
