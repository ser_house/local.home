<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Oga;


use App\Domain\DateTimeService;
use App\Domain\Oga\HistoryBuilder;
use App\Domain\Oga\IStorage;
use App\Domain\Oga\UI\Presenter;
use App\Framework\Oga\TimeFormatter;
use App\Http\Controllers\Controller;


/**
 * Class Home
 *
 * @package App\Http\Controllers\Web\Oga
 */
class Home extends Controller {

  private DateTimeService $dateTimeService;
  private TimeFormatter $timeFormatter;
  private IStorage $storage;
  private HistoryBuilder $historyBuilder;
  private Presenter $presenter;

  /**
   * Home constructor.
   *
   * @param DateTimeService $dateTimeService
   * @param TimeFormatter $timeFormatter
   * @param IStorage $storage
   * @param HistoryBuilder $historyBuilder
   * @param Presenter $presenter
   */
  public function __construct(DateTimeService $dateTimeService, TimeFormatter $timeFormatter, IStorage $storage, HistoryBuilder $historyBuilder, Presenter $presenter) {
    $this->dateTimeService = $dateTimeService;
    $this->timeFormatter = $timeFormatter;
    $this->storage = $storage;
    $this->historyBuilder = $historyBuilder;
    $this->presenter = $presenter;
  }


  /**
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
   * @throws \Exception
   */
  public function __invoke() {

    $today = $this->dateTimeService->getNowAsLocal();

    $last_week_days = $this->historyBuilder->buildLastWeekDays($today);

    $lastDateTime = $this->storage->getLastDateTime();
    if ($lastDateTime) {
      $lastDateTime = $lastDateTime->setTimezone($this->dateTimeService->getLocalTimezone());
    }

    $new_is_allowed = (null === $lastDateTime) || $lastDateTime->format('Y-m-d') !== $today->format('Y-m-d');

    $avg_items = $this->storage->getAvrDaysByYears();

    return view('oga.home', [
      'new_is_allowed' => $new_is_allowed,
      'date' => $new_is_allowed ? $today->format('Y-m-d') : null,
      'time' => $new_is_allowed ? $this->timeFormatter->roundedHoursMinutes($today) : null,
      'last_items' => $this->presenter->presentOgaDateItems(...$last_week_days),
      'avg_items' => $this->presenter->presentItems(...$avg_items),
    ]);
  }
}
