<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Oga;


use App\Framework\Oga\CalendarService;
use App\Http\Controllers\Controller;


/**
 * Class Calendar
 *
 * @package App\Http\Controllers\Web\Oga
 */
class Calendar extends Controller {

  public function __construct(private readonly CalendarService $calendarService) {

  }

  /**
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
   * @throws \Exception
   */
  public function __invoke() {
    $data = $this->calendarService->getYearData();

    return view('oga.calendar', [
      'data' => $data,
    ]);
  }
}
