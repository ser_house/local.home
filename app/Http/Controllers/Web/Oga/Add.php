<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Oga;


use App\Framework\Oga\AddService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


/**
 * Class Add
 *
 * @package App\Http\Controllers\Web\Oga
 */
class Add extends Controller {

  private AddService $addService;

  /**
   * Add constructor.
   *
   * @param AddService $addService
   */
  public function __construct(AddService $addService) {
    $this->addService = $addService;
  }

  /**
   * @param Request $request
   *
   * @return \Illuminate\Http\RedirectResponse
   * @throws \Exception
   */
  public function __invoke(Request $request) {
    $date = $request->input('date');
    $time = $request->input('time');

    $dt_str = "$date $time";
    $days = $this->addService->addByLocalString($dt_str);

    return back()->withInput([
      'date' => $date,
      'time' => $time,
    ])->with('success', "Время '$dt_str' добавлено. Дней: $days");
  }
}
