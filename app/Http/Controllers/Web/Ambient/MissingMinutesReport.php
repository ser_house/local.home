<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Ambient;


use App\Domain\Ambient\MissingMinutes\UI\Presenter;
use App\Framework\Ambient\MissingMinutesService;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 * Class MissingMinutesReport
 *
 * @package App\Http\Controllers\Web\Ambient
 */
class MissingMinutesReport extends Controller {

  private MissingMinutesService $missingMinutesService;
  private Presenter $presenter;

  /**
   * MissingMinutesReport constructor.
   *
   * @param MissingMinutesService $missingMinutesService
   * @param Presenter $presenter
   */
  public function __construct(MissingMinutesService $missingMinutesService, Presenter $presenter) {
    $this->missingMinutesService = $missingMinutesService;
    $this->presenter = $presenter;
  }


  /**
   *
   * @return Application|Factory|View
   * @throws \Exception
   */
  public function __invoke() {
    $missing_minutes = $this->missingMinutesService->getCountForAllTime();

    return view('ambient.missing_minutes_report', [
      'items' => $this->presenter->presentMissingMinutesItems(...$missing_minutes),
    ]);
  }
}
