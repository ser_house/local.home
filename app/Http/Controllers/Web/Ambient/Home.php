<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Ambient;


use App\Domain\Ambient\IAmbientStorage;
use App\Domain\Ambient\Sensor;
use App\Domain\Ambient\UI\MeasurePresenter;
use App\Domain\Common\UI\DateTimeFormatter\DateTimeFormatter;
use App\Domain\Common\UI\DateTimeFormatter\TimeFormatter;
use App\Domain\INotifyPusher;
use App\Framework\Ambient\MissingMinutesService;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;


/**
 * Class Home
 *
 * @package App\Http\Controllers\Web\Ambient
 */
class Home extends Controller {

  /**
   * Home constructor.
   *
   * @param IAmbientStorage $ambientStorage
   * @param MissingMinutesService $missingMinutesService
   * @param MeasurePresenter $measurePresenter
   */
  public function __construct(
    private IAmbientStorage $ambientStorage,
    private MissingMinutesService $missingMinutesService,
    private MeasurePresenter $measurePresenter,
    private INotifyPusher $notifyPusher,
  ) {

  }


  /**
   *
   * @return Application|Factory|View
   * @throws \Exception
   */
  public function __invoke() {
    $today_missing_count = $this->missingMinutesService->getCountForToday();

    $outdoors = $this->ambientStorage->getLastBySensorId(Sensor::OUTDOORS1);
    $indoors = $this->ambientStorage->getLastBySensorId(Sensor::ROOM1);

    $timeFormatter = new TimeFormatter();

    $last = null;
    if ($indoors) {
      $last = [
        Sensor::OUTDOORS1 => $this->measurePresenter->present($timeFormatter, $outdoors),
        Sensor::ROOM1 => $this->measurePresenter->present($timeFormatter, $indoors),
      ];
    }

    $last_day_items = $this->ambientStorage->getAvgByHours(25);
    $last_hour_items = $this->ambientStorage->getLastHour();
    $dateTimeFormatter = new DateTimeFormatter();
    $last_day_views = $this->measurePresenter->presentViews($dateTimeFormatter, ...$last_day_items);
    $last_hour_views = $this->measurePresenter->presentViews($timeFormatter, ...$last_hour_items);

    return view('ambient.home', [
      'ws_host' => config('app.ws_host'),
      'ws_port' => config('app.ws_port'),
      'token' => $this->notifyPusher->getToken(),
      'last' => $last,
      'last_day_items' => $last_day_views,
      'last_hour_items' => $last_hour_views,
      'today_missing_count' => $today_missing_count,
    ]);
  }
}
