<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Plants;


use App\Domain\DateTimeService;
use App\Domain\Plants\PlantWateringSchedule;
use App\Domain\Plants\UI\Presenter;
use App\Http\Controllers\Controller;
use App\Models\Plant;
use DateTimeImmutable;
use Illuminate\Contracts\View\View;


/**
 * Class Home
 *
 * @package App\Http\Controllers\Web\Plants
 */
class Home extends Controller {
  private PlantWateringSchedule $plantWateringSchedule;
  private DateTimeService $dateTimeService;
  private Presenter $presenter;

  /**
   * Home constructor.
   *
   * @param PlantWateringSchedule $plantWateringSchedule
   * @param DateTimeService $dateTimeService
   * @param Presenter $presenter
   */
  public function __construct(PlantWateringSchedule $plantWateringSchedule, DateTimeService $dateTimeService, Presenter $presenter) {
    $this->plantWateringSchedule = $plantWateringSchedule;
    $this->dateTimeService = $dateTimeService;
    $this->presenter = $presenter;
  }

  /**
   *
   * @return View
   * @throws \Exception
   */
  public function __invoke() {
    $plants = Plant::where('active', true)->orderBy('title', 'asc')->get();

    $periodicity = $this->plantWateringSchedule->periodicity();

    $tz = $this->dateTimeService->getLocalTimezone();

    $data = [];
    /** @var Plant $plant */
    foreach ($plants as $plant) {
      $lastWateringDate = new DateTimeImmutable($plant->last_watering_date, $tz);
      if ($plant->last_watering_date) {
        $firstDate = $lastWateringDate;
      }
      else {
        $firstDate = new DateTimeImmutable($plant->next_watering_date, $tz);
      }

      $schedule_dates = $this->plantWateringSchedule->generate($firstDate, $lastWateringDate, $plant->summer_periodicity, $plant->winter_periodicity);
      $dateViews = $this->presenter->presentPlantDateItems(...$schedule_dates);

      $item = $plant->getAttributes();

      $item['summer_periodicity'] = $periodicity[$plant->summer_periodicity];
      $item['winter_periodicity'] = $periodicity[$plant->winter_periodicity];
      $item['days'] = $dateViews;
      $data[] = $item;
    }

    return view('plant.home', [
      'plants' => $data,
    ]);
  }
}
