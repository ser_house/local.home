<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Plants;


use App\Http\Controllers\Controller;
use App\Models\Plant;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;


/**
 * Class Add
 *
 * @package App\Http\Controllers\Web\Plants
 */
class Add extends Controller {

  /**
   * @param Request $request
   *
   * @return Application|RedirectResponse|Redirector
   */
  public function __invoke(Request $request) {
    $title = $request->get('title');
    $description = $request->get('description');
    $summer_periodicity = $request->get('summer_periodicity');
    $winter_periodicity = $request->get('winter_periodicity');
    $next_watering_date = $request->get('next_watering_date');
    $active = $request->get('active', false);

    $plant = Plant::create([
      'title' => $title,
      'description' => $description,
      'summer_periodicity' => $summer_periodicity,
      'winter_periodicity' => $winter_periodicity,
      'next_watering_date' => $next_watering_date,
      'active' => $active,
    ]);

    $msg = "Растение '$plant->title' добавлено.";

    return redirect('plants')->with('success', $msg);
  }
}
