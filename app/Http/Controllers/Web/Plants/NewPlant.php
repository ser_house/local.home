<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.09.2019
 * Time: 12:21
 */


namespace App\Http\Controllers\Web\Plants;


use App\Domain\DateTimeService;
use App\Domain\Plants\PlantWateringSchedule;
use App\Http\Controllers\Controller;


/**
 * Class NewPlant
 *
 * @package App\Http\Controllers\Web\Plants
 */
class NewPlant extends Controller {

  private PlantWateringSchedule $plantWateringSchedule;
  private DateTimeService $dateTimeService;

  /**
   * NewPlant constructor.
   *
   * @param \App\Domain\Plants\PlantWateringSchedule $plantWateringSchedule
   * @param DateTimeService $dateTimeService
   */
  public function __construct(PlantWateringSchedule $plantWateringSchedule, DateTimeService $dateTimeService) {
    $this->plantWateringSchedule = $plantWateringSchedule;
    $this->dateTimeService = $dateTimeService;
  }


  /**
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
   * @throws \Exception
   */
  public function __invoke() {

    $periodicity = $this->plantWateringSchedule->periodicity();

    $today = $this->dateTimeService->getNowAsLocal();
    $today_is_summer = $this->dateTimeService->isSummerDate($today);

    $form_data = [
      'id' => '',
      'title' => '',
      'description' => '',
      'periodicity' => $periodicity,
      'summer_periodicity' => 1,
      'winter_periodicity' => 2,
      'active' => true,
      'today_is_summer' => $today_is_summer,
    ];

    return view('plant.new', [
      'data' => $form_data,
    ]);
  }
}
