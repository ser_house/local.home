<?php

namespace App\Jobs;


use App\Domain\Ambient\IAmbientStorage;
use App\Domain\Ambient\TemperatureNormalizer;
use App\Domain\Ambient\UI\MeasurePresenter;
use App\Domain\Ambient\UI\WebSocketData;
use App\Domain\Common\UI\DateTimeFormatter\DateTimeFormatter;
use App\Domain\Common\UI\DateTimeFormatter\TimeFormatter;
use App\Domain\INotifyPusher;
use DateTimeImmutable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class AddAmbient implements ShouldQueue {
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * @param TemperatureNormalizer $temperatureNormalizer
   * @param DateTimeImmutable $systemDateTime
   * @param array $outdoors_data
   * @param array $indoors_data
   */
  public function __construct(
    private TemperatureNormalizer $temperatureNormalizer,
    private DateTimeImmutable $systemDateTime,
    private array $outdoors_data,
    private array $indoors_data
  ) {
  }


  /**
   * @param IAmbientStorage $ambientStorage
   * @param INotifyPusher $notifyPusher
   * @param MeasurePresenter $measurePresenter
   */
  public function handle(
    IAmbientStorage $ambientStorage,
    INotifyPusher $notifyPusher,
    MeasurePresenter $measurePresenter
  ) {
    if (!empty($this->indoors_data)) {

      $indoorsMeasure = $ambientStorage->add(
        $this->systemDateTime,
        (int)$this->indoors_data['sid'],
        (float)$this->indoors_data['t'],
        (float)$this->indoors_data['h']
      );

      $outdoorsMeasure = null;
      if (!empty($this->outdoors_data)) {
        $out_t = $this->temperatureNormalizer->normalize($this->outdoors_data['t']);
        $outdoorsMeasure = $ambientStorage->add($this->systemDateTime, (int)$this->outdoors_data['sid'], $out_t);
      }

      $timeFormatter = new TimeFormatter();
      $views = $measurePresenter->presentViews($timeFormatter, $indoorsMeasure, $outdoorsMeasure);

      $toSend = new WebSocketData(...$views);
      $notifyPusher->send('last_ambient', $toSend);

      $last_day_items = $ambientStorage->getAvgByHours(25);
      $last_hour_items = $ambientStorage->getLastHour();
      $dateTimeFormatter = new DateTimeFormatter();
      $last_day_views = $measurePresenter->presentViews($dateTimeFormatter, ...$last_day_items);
      $last_hour_views = $measurePresenter->presentViews($timeFormatter, ...$last_hour_items);

      $toSend = new WebSocketData(...$last_day_views);
      $notifyPusher->send('last_day_update', $toSend);

      $toSend = new WebSocketData(...$last_hour_views);
      $notifyPusher->send('last_hour_update', $toSend);

      Log::channel('ambient')->info('Job:AddAmbient handled data', [
        'system_time' => $this->systemDateTime->format('d.m.Y H:i:s'),
        'indoors' => $this->indoors_data,
        'outdoors' => $this->outdoors_data,
      ]);
    }
    else {
      Log::channel('ambient')->warning('Job:AddAmbient data is empty');
    }
  }
}
