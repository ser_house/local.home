<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 10:42
 */
declare(strict_types=1);

namespace App\Domain\Weight\UI;

use App\Domain\Common\UI\LabelValueView;
use App\Domain\Common\UI\Presenter as CommonPresenter;
use App\Domain\Weight\DateItem;

/**
 * Class Presenter
 *
 * @package App\Domain\Weight\UI
 */
class Presenter extends CommonPresenter {

  /**
   * @param DateItem ...$dateItems
   *
   * @return array
   */
  public function presentWeightDateItems(DateItem ...$dateItems): array {
    $views = [];
    foreach ($dateItems as $dateItem) {
      $views[] = $this->presentWeightDateItem($dateItem);
    }

    return $views;
  }

  /**
   * @param DateItem $dateItem
   *
   * @return LabelValueView
   */
  public function presentWeightDateItem(DateItem $dateItem): LabelValueView {
    return new LabelValueView(
      $dateItem->date()->format('d.m.Y'),
      number_format($dateItem->value(), 1, ',', ' ')
    );
  }
}
