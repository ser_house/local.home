<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 10:37
 */

declare(strict_types=1);

namespace App\Domain\Weight;

use App\Domain\Common\DateItem as CommonDateItem;
use DateTimeImmutable;

/**
 * Class DateItem
 *
 * @package App\Domain\Weight
 */
class DateItem extends CommonDateItem {

  private float $value;

  /**
   * DateItem constructor.
   *
   * @param DateTimeImmutable $date
   * @param DateTimeImmutable $today
   * @param float $value
   */
  public function __construct(DateTimeImmutable $date, DateTimeImmutable $today, float $value) {
    parent::__construct($date, $today);

    $this->value = $value;
  }

  /**
   * @return float
   */
  public function value(): float {
    return $this->value;
  }
}
