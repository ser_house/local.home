<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.07.2021
 * Time: 13:59
 */


namespace App\Domain\Weight;


use App\Domain\DateTimeService;
use App\Domain\Weight\UI\Presenter;

class NewIsAllowChecker {
  private DateTimeService $dateTimeService;
  private IStorage $storage;


  /**
   * NewIsAllowChecker constructor.
   *
   * @param DateTimeService $dateTimeService
   * @param IStorage $storage
   * @param Presenter $presenter
   */
  public function __construct(DateTimeService $dateTimeService, IStorage $storage) {
    $this->dateTimeService = $dateTimeService;
    $this->storage = $storage;
  }

  public function isAllow(): bool {
    $today = $this->dateTimeService->getNowAsLocal();

    $lastDateTime = $this->storage->getLastDateTime();

    $lastIsToday = $lastDateTime && $lastDateTime->format('Y-m-d') === $today->format('Y-m-d');

    return (1 === (int)$today->format('d')) && !$lastIsToday;
  }
}
