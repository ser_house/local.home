<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.07.2021
 * Time: 17:38
 */


namespace App\Domain\Weight;


use App\Domain\Common\LabelValueItem;
use DateTimeImmutable;

/**
 * Interface IStorage
 *
 * @package App\Domain\Weight
 */
interface IStorage {
  /**
   * @param DateTimeImmutable $dateTime
   * @param float $value
   */
  public function add(DateTimeImmutable $dateTime, float $value): void;

  /**
   *
   * @return DateTimeImmutable|null
   */
  public function getLastDateTime(): ?DateTimeImmutable;

  /**
   * @param int $num
   *
   * @return DateItem[]
   */
  public function getLastItems(int $num = 5): array;

  /**
   *
   * @return LabelValueItem[]
   */
  public function getAvrValuesByYears(): array;
}
