<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.07.2021
 * Time: 17:39
 */


namespace App\Domain\Weight;

use DateTimeImmutable;

/**
 * Class WeightItem
 *
 * @package App\Domain\Weight
 */
class WeightItem {
  protected DateTimeImmutable $date;
  protected float $value;

  /**
   * WeightItem constructor.
   *
   * @param DateTimeImmutable $date
   * @param float $value
   */
  public function __construct(DateTimeImmutable $date, float $value) {
    $this->date = $date;
    $this->value = $value;
  }


  /**
   * @return DateTimeImmutable
   */
  public function date(): DateTimeImmutable {
    return $this->date;
  }

  /**
   * @return int
   */
  public function value(): int {
    return $this->value;
  }
}
