<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.06.2021
 * Time: 7:47
 */


namespace App\Domain\Plants;


use App\Domain\Common\DateItem;
use App\Domain\Plants\DateItem as PlantsDayItem;
use App\Domain\Common\IDayBuilder;
use DateTimeImmutable;

/**
 * Class DayBuilder
 *
 * @package App\Domain\Plants
 */
class DayBuilder implements IDayBuilder {
  private DateTimeImmutable $today;
  private DateTimeImmutable $lastWateringDate;

  /**
   * DayBuilder constructor.
   *
   * @param DateTimeImmutable $today
   * @param DateTimeImmutable $lastWateringDate
   */
  public function __construct(DateTimeImmutable $today, DateTimeImmutable $lastWateringDate) {
    $this->today = $today;
    $this->lastWateringDate = $lastWateringDate;
  }


  /**
   * @inheritDoc
   */
  public function buildDayItem(DateTimeImmutable $date): DateItem {
    return new PlantsDayItem($date, $this->today, $this->lastWateringDate);
  }

}
