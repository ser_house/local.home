<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 11:42
 */


namespace App\Domain\Plants\UI;

use App\Domain\Common\UI\Presenter as CommonPresenter;
use App\Domain\Plants\DateItem;

/**
 * Class Presenter
 *
 * @package App\Domain\Plants\UI
 */
class Presenter extends CommonPresenter {

  /**
   * @param DateItem ...$dateItems
   *
   * @return array
   */
  public function presentPlantDateItems(DateItem ...$dateItems): array {
    $views = [];
    foreach ($dateItems as $dateItem) {
      $views[] = $this->presentPlantDateItem($dateItem);
    }

    return $views;
  }

  /**
   * @param DateItem $dateItem
   *
   * @return DateView
   */
  public function presentPlantDateItem(DateItem $dateItem): DateView {
    $commonView = $this->presentDateItem($dateItem);
    $view = DateView::buildFromCommon($commonView);
    $view->is_watering_day = $dateItem->isWateringDay();
    if ($dateItem->isWateringDay()) {
      $view->css_class .= ' is-watering-day';
    }

    return $view;
  }
}
