<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.05.2021
 * Time: 20:14
 */


namespace App\Domain\Plants;

use App\Domain\Common\CalendarBuilder;
use App\Domain\DateTimeService;
use DateInterval;
use DateTimeImmutable;
use Exception;
use function last;

/**
 * Class PlantWateringSchedule
 *
 * @package App\Domain\Plants
 */
class PlantWateringSchedule {

  private const KEY_DATETIME_FORMAT = 'Ymd';

  private DateTimeService $dateTimeService;
  private CalendarBuilder $calendarBuilder;

  /**
   * PlantWateringSchedule constructor.
   *
   * @param DateTimeService $dateTimeService
   * @param CalendarBuilder $calendarBuilder
   */
  public function __construct(DateTimeService $dateTimeService, CalendarBuilder $calendarBuilder) {
    $this->dateTimeService = $dateTimeService;
    $this->calendarBuilder = $calendarBuilder;
  }


  /**
   * @param DateTimeImmutable $firstWateringDate
   * @param DateTimeImmutable $lastWateringDate
   * @param int $summer_interval
   * @param int $winter_interval
   * @param int $weeks_count
   *
   * @return array
   * @throws Exception
   */
  public function generate(DateTimeImmutable $firstWateringDate, DateTimeImmutable $lastWateringDate, int $summer_interval, int $winter_interval, int $weeks_count = 1): array {
    // Строится следующая неделя (при $weeks_count === 1), начиная с сегодняшнего дня.
    $today = $this->dateTimeService->getNowAsLocal();
    $dayBuilder = new DayBuilder($today, $lastWateringDate);
    $dates = $this->calendarBuilder->buildWeeks($today, $dayBuilder, $weeks_count);

    $last_weeks_date_key = last(array_keys($dates));

    // Переменная дата полива, первый полив - в дату из аргумента метода.
    $date = $firstWateringDate;

    // Если первая дата полива входит в построенную неделю, то отмечаем её таковой.
    $key = $date->format(self::KEY_DATETIME_FORMAT);
    if (isset($dates[$key])) {
      $dates[$key]->setIsWateringDay();
    }

    // Ищем следующие даты полива в построенной неделе и отмечаем найденные.
    while ($key <= $last_weeks_date_key) {
      $date = $this->nextWateringDate($date, $summer_interval, $winter_interval);
      $key = $date->format(self::KEY_DATETIME_FORMAT);
      if (isset($dates[$key])) {
        $dates[$key]->setIsWateringDay();
      }
    }

    return $dates;
  }

  /**
   * @param DateTimeImmutable $lastWateringDate
   * @param int $summer_interval
   * @param int $winter_interval
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function nextWateringDate(DateTimeImmutable $lastWateringDate, int $summer_interval, int $winter_interval): DateTimeImmutable {
    return $this->dateTimeService->isSummerDate($lastWateringDate)
      ? $lastWateringDate->add(new DateInterval("P{$summer_interval}D"))
      : $lastWateringDate->add(new DateInterval("P{$winter_interval}D"));
  }

  /**
   *
   * @return array
   */
  public function periodicity(): array {
    $periodicity = [];
    for ($i = 1; $i <= 7; $i++) {
      $periodicity[$i] = "Раз в $i " . $this->morph($i, 'день', 'дня', 'дней');
    }

    return $periodicity;
  }

  /**
   * @param int $n
   * @param string $f1
   * @param string $f2
   * @param string $f5
   *
   * @return string
   */
  private function morph(int $n, string $f1, string $f2, string $f5): string {
    $n = abs($n) % 100;
    if ($n > 10 && $n < 20) {
      return $f5;
    }
    $n %= 10;
    if ($n > 1 && $n < 5) {
      return $f2;
    }
    if ($n == 1) {
      return $f1;
    }

    return $f5;
  }
}
