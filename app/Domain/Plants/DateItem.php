<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.06.2021
 * Time: 8:22
 */


namespace App\Domain\Plants;


use App\Domain\Common\DateItem as CommonDateItem;
use DateTimeImmutable;

/**
 * Class DateItem
 *
 * @package App\Domain\Plants\UI
 */
class DateItem extends CommonDateItem {

  private bool $is_watering_day;

  /**
   * DateItem constructor.
   *
   * @param DateTimeImmutable $date
   * @param DateTimeImmutable $today
   * @param DateTimeImmutable $lastWateringDate
   */
  public function __construct(DateTimeImmutable $date, DateTimeImmutable $today, DateTimeImmutable $lastWateringDate) {
    parent::__construct($date, $today);

    if ($date->format(self::COMPARE_DATE_FORMAT) === $lastWateringDate->format(self::COMPARE_DATE_FORMAT)) {
      $this->status = self::IS_PAST;
    }

    $this->is_watering_day = false;
  }

  /**
   * @return bool
   */
  public function isWateringDay(): bool {
    return $this->is_watering_day;
  }

  /**
   * @return DateItem
   */
  public function setIsWateringDay(): DateItem {
    $this->is_watering_day = true;

    return $this;
  }
}
