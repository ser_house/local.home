<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.05.2021
 * Time: 13:14
 */


namespace App\Domain;

use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use Exception;

/**
 * Class DateTimeService
 *
 * @package App\Domain
 */
class DateTimeService {
  private DateTimeZone $localTimezone;
  private DateTimeZone $systemTimezone;

  /**
   * DateTimeService constructor.
   *
   * @param string $local_timezone
   */
  public function __construct(string $local_timezone) {
    $this->localTimezone = new DateTimeZone($local_timezone);
    $this->systemTimezone = new DateTimeZone('UTC');
  }

  /**
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function getNow(): DateTimeImmutable {
    return new DateTimeImmutable('now', $this->getSystemTimezone());
  }

  /**
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function getNowAsLocal(): DateTimeImmutable {
    return new DateTimeImmutable('now', $this->getLocalTimezone());
  }

  /**
   *
   * @return DateTimeZone
   */
  public function getLocalTimezone(): DateTimeZone {
    return $this->localTimezone;
  }

  /**
   *
   * @return DateTimeZone
   */
  public function getSystemTimezone(): DateTimeZone {
    return $this->systemTimezone;
  }

  /**
   * @param string $time
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function getSystemDateTimeFromLocalStr(string $time): DateTimeImmutable {
    $dateTime = $this->getLocalDateTimeFromLocalStr($time);

    return $dateTime->setTimezone($this->getSystemTimezone());
  }

  /**
   * @param DateTimeImmutable $dateFirst
   * @param DateTimeImmutable $dateLast
   *
   * @return int
   */
  public function getDiffDays(DateTimeImmutable $dateFirst, DateTimeImmutable $dateLast): int {
    $firstDateTime = DateTime::createFromImmutable($dateFirst);
    $firstDateTime->setTime(0, 0);

    $lastDateTime = DateTime::createFromImmutable($dateLast);
    $lastDateTime->setTime(0, 0);

    return $lastDateTime->diff($firstDateTime)->d;
  }

  /**
   * @param string $time
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function getSystemDateTimeFromSystemStr(string $time): DateTimeImmutable {
    return new DateTimeImmutable($time, $this->getSystemTimezone());
  }

  /**
   * @param string $time
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function getLocalDateTimeFromLocalStr(string $time): DateTimeImmutable {
    return new DateTimeImmutable($time, $this->getLocalTimezone());
  }

  /**
   * @param string $system_time
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function getLocalDateTimeFromSystemStr(string $system_time): DateTimeImmutable {
    $systemDateTime = new DateTimeImmutable($system_time);
    return $this->systemDateTimeToLocal($systemDateTime);
  }

  /**
   * @param DateTimeImmutable $date
   *
   * @return bool
   * @throws Exception
   */
  public function isSummerDate(DateTimeImmutable $date): bool {
    $tz = $this->getLocalTimezone();
    $startSummerDate = new DateTimeImmutable('March 15', $tz);
    $startWinterDate = new DateTimeImmutable('October 15', $tz);

    return $date >= $startSummerDate && $date < $startWinterDate;
  }

  /**
   *
   * @return string
   */
  public function getLocalTimezoneName(): string {
    return $this->localTimezone->getName();
  }

  /**
   * @param DateTimeImmutable $localDateTime
   *
   * @return DateTimeImmutable
   */
  public function localDateTimeToSystem(DateTimeImmutable $localDateTime): DateTimeImmutable {
    if ($localDateTime->getTimezone() === $this->getSystemTimezone()) {
      return $localDateTime;
    }

    return $localDateTime->setTimezone($this->getSystemTimezone());
  }

  /**
   * @param DateTimeImmutable $systemDateTime
   *
   * @return DateTimeImmutable
   */
  public function systemDateTimeToLocal(DateTimeImmutable $systemDateTime): DateTimeImmutable {
    if ($systemDateTime->getTimezone() === $this->getLocalTimezone()) {
      return $systemDateTime;
    }

    return $systemDateTime->setTimezone($this->getLocalTimezone());
  }
}
