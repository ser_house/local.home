<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.06.2021
 * Time: 7:27
 */


namespace App\Domain\Common;

use DateInterval;
use DateTimeImmutable;

/**
 * Class CalendarBuilder
 *
 * @package App\Domain\Common
 */
class CalendarBuilder {

  private const KEY_DATETIME_FORMAT = 'Ymd';

  /**
   * @param DateTimeImmutable $firstDate
   * @param IDayBuilder $dayBuilder
   * @param int $weeks_count
   *
   * @return array
   */
  public function buildWeeks(DateTimeImmutable $firstDate, IDayBuilder $dayBuilder, int $weeks_count): array {
    $date = $firstDate;
    $key = $date->format(self::KEY_DATETIME_FORMAT);

    $dates = [
      $key => $dayBuilder->buildDayItem($date),
    ];

    $interval = new DateInterval('P1D');

    $built_weeks_count = 0;
    while ($built_weeks_count < $weeks_count) {
      for ($i = 1; $i < 7; $i++) {

        $date = $date->add($interval);

        $key = $date->format(self::KEY_DATETIME_FORMAT);
        $dates[$key] = $dayBuilder->buildDayItem($date);
      }
      $built_weeks_count++;
    }

    return $dates;
  }
}
