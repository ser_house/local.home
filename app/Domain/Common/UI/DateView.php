<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.06.2021
 * Time: 20:22
 */


namespace App\Domain\Common\UI;


/**
 * Class DateView
 *
 * @package App\Domain\Common\UI
 */
class DateView {
  public string $date;
  public string $weekday;
  public string $status;
  public string $css_class;
}
