<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 10:08
 */


namespace App\Domain\Common\UI;


/**
 * Class LabelValueView
 *
 * @package App\Domain\Common\UI
 */
class LabelValueView {
  public string $label;
  public string $value;

  /**
   * LabelValueView constructor.
   *
   * @param string $label
   * @param string $value
   */
  public function __construct(string $label, string $value) {
    $this->label = $label;
    $this->value = $value;
  }
}
