<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.07.2021
 * Time: 6:45
 */


namespace App\Domain\Common\UI\DateTimeFormatter;


use DateTimeImmutable;

class TimeFormatter implements IDateTimeFormatter {
  /**
   * @inheritDoc
   */
  public function format(DateTimeImmutable $dateTime): string {
    return $dateTime->format('H:i');
  }

}
