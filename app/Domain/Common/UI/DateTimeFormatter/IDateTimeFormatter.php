<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.07.2021
 * Time: 6:40
 */


namespace App\Domain\Common\UI\DateTimeFormatter;


use DateTimeImmutable;

interface IDateTimeFormatter {

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return string
   */
  public function format(DateTimeImmutable $dateTime): string;
}
