<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 10:14
 */


namespace App\Domain\Common\UI;


use App\Domain\Common\DateItem;
use App\Domain\Common\LabelValueItem;

/**
 * Class Presenter
 *
 * @package App\Domain\Common\UI
 */
class Presenter {

  /**
   * @param LabelValueItem ...$items
   *
   * @return LabelValueView[]
   */
  public function presentItems(LabelValueItem ...$items): array {
    $views = [];
    foreach ($items as $item) {
      $views[] = $this->presentItem($item);
    }

    return $views;
  }

  /**
   * @param LabelValueItem $item
   *
   * @return LabelValueView
   */
  public function presentItem(LabelValueItem $item): LabelValueView {
    return new LabelValueView($item->label(), $item->value());
  }

  /**
   * @param DateItem $dateItem
   *
   * @return DateView
   */
  public function presentDateItem(DateItem $dateItem): DateView {
    $view = new DateView();
    $view->date = $dateItem->date()->format('d.m.Y');
    $view->weekday = $dateItem->weekdayName();
    $view->status = $dateItem->status();
    $view->css_class = str_replace('_', '-', $dateItem->status());

    return $view;
  }
}
