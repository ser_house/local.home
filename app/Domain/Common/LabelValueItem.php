<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 10:08
 */


namespace App\Domain\Common;


/**
 * Class LabelValueItem
 *
 * @package App\Domain\Common
 */
class LabelValueItem {
  private string $label;
  private string $value;

  /**
   * LabelValueItem constructor.
   *
   * @param string $label
   * @param string $value
   */
  public function __construct(string $label, string $value) {
    $this->label = $label;
    $this->value = $value;
  }

  /**
   * @return string
   */
  public function label(): string {
    return $this->label;
  }

  /**
   * @return string
   */
  public function value(): string {
    return $this->value;
  }
}
