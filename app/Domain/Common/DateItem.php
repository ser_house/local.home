<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.06.2021
 * Time: 7:32
 */


namespace App\Domain\Common;

use DateTimeImmutable;

class DateItem {
  public const WEEKDAY_NAMES_RU = [
    1 => 'Пн',
    2 => 'Вт',
    3 => 'Ср',
    4 => 'Чт',
    5 => 'Пт',
    6 => 'Сб',
    7 => 'Вс',
  ];

  public const IS_PAST = 'is_past';
  public const IS_FUTURE = 'is_future';
  public const IS_TODAY = 'is_today';

  protected const COMPARE_DATE_FORMAT = 'Ymd';

  protected DateTimeImmutable $date;
  protected string $weekday_name;
  protected string $status;

  /**
   * DateItem constructor.
   *
   * @param DateTimeImmutable $date
   * @param DateTimeImmutable $today
   */
  public function __construct(DateTimeImmutable $date, DateTimeImmutable $today) {
    $this->date = $date;
    $this->status = $this->calcDayStatus($date, $today);
    $this->weekday_name = self::WEEKDAY_NAMES_RU[$this->date->format('N')];
  }

  /**
   * @param DateTimeImmutable $date
   * @param DateTimeImmutable $today
   *
   * @return string
   */
  private function calcDayStatus(DateTimeImmutable $date, DateTimeImmutable $today): string {
    $today_compare_str = $today->format(self::COMPARE_DATE_FORMAT);
    $date_compare_str = $date->format(self::COMPARE_DATE_FORMAT);

    if ($date_compare_str < $today_compare_str) {
      return self::IS_PAST;
    }

    if ($date_compare_str > $today_compare_str) {
      return self::IS_FUTURE;
    }

    return self::IS_TODAY;
  }

  /**
   * @return DateTimeImmutable
   */
  public function date(): DateTimeImmutable {
    return $this->date;
  }

  /**
   * @return string
   */
  public function weekdayName(): string {
    return $this->weekday_name;
  }

  /**
   * @return string
   */
  public function status(): string {
    return $this->status;
  }
}
