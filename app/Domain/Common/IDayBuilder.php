<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.06.2021
 * Time: 7:30
 */


namespace App\Domain\Common;

use DateTimeImmutable;

/**
 * Interface IDayBuilder
 *
 * @package App\Domain\Common
 */
interface IDayBuilder {

  /**
   * @param DateTimeImmutable $date
   *
   * @return DateItem
   */
  public function buildDayItem(DateTimeImmutable $date): DateItem;
}
