<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 11:30
 */


namespace App\Domain\Oga\UI;

use App\Domain\Common\UI\Presenter as CommonPresenter;
use App\Domain\Oga\DateItem;


/**
 * Class Presenter
 *
 * @package App\Domain\Oga\UI
 */
class Presenter extends CommonPresenter {

  /**
   * @param DateItem ...$dateItems
   *
   * @return array
   */
  public function presentOgaDateItems(DateItem ...$dateItems): array {
    $views = [];
    foreach ($dateItems as $dateItem) {
      $views[] = $this->presentOgaDateItem($dateItem);
    }

    return $views;
  }

  /**
   * @param DateItem $dateItem
   *
   * @return DateView
   */
  public function presentOgaDateItem(DateItem $dateItem): DateView {
    $commonView = $this->presentDateItem($dateItem);
    $view = DateView::buildFromCommon($commonView);

    $has_days = null !== $dateItem->days();

    if ($has_days) {
      $view->date = $dateItem->date()->format('d.m.Y H:i');
    }
    $view->days = $has_days ? (string)$dateItem->days() : '';

    if ($has_days) {
      $view->css_class .= ' has-data';
    }

    return $view;
  }
}
