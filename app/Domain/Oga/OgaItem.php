<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.06.2021
 * Time: 8:44
 */


namespace App\Domain\Oga;

use DateTimeImmutable;

/**
 * Class OgaItem
 *
 * @package App\Domain\Oga
 */
class OgaItem {
  protected DateTimeImmutable $date;
  protected int $days;

  /**
   * OgaItem constructor.
   *
   * @param DateTimeImmutable $date
   * @param int $days
   */
  public function __construct(DateTimeImmutable $date, int $days) {
    $this->date = $date;
    $this->days = $days;
  }

  /**
   * @return DateTimeImmutable
   */
  public function date(): DateTimeImmutable {
    return $this->date;
  }

  /**
   * @return int
   */
  public function days(): int {
    return $this->days;
  }
}
