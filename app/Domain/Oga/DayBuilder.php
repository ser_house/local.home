<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.06.2021
 * Time: 7:47
 */


namespace App\Domain\Oga;


use App\Domain\Common\DateItem;
use App\Domain\Common\IDayBuilder;
use DateTimeImmutable;

/**
 * Class DayBuilder
 *
 * @package App\Domain\Oga
 */
class DayBuilder implements IDayBuilder {
  private DateTimeImmutable $today;
  /** @var OgaItem[] */
  private array $oga_items = [];

  /**
   * DayBuilder constructor.
   *
   * @param DateTimeImmutable $today
   * @param OgaItem[] $oga_items
   */
  public function __construct(DateTimeImmutable $today, array $oga_items) {
    $this->today = $today;
    foreach ($oga_items as $ogaItem) {
      $key = $ogaItem->date()->format('Ymd');
      $this->oga_items[$key] = $ogaItem;
    }
  }


  /**
   * @inheritDoc
   */
  public function buildDayItem(DateTimeImmutable $date): DateItem {
    $key = $date->format('Ymd');
    $days = null;
    if (isset($this->oga_items[$key])) {
      $ogaItem = $this->oga_items[$key];
      $days = $ogaItem->days();
      $date = $ogaItem->date();
    }

    return new \App\Domain\Oga\DateItem($date, $this->today, $days);
  }

}
