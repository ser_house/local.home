<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2021
 * Time: 13:00
 */


namespace App\Domain\Oga;

use App\Domain\Common\LabelValueItem;
use DateTimeImmutable;

/**
 * Interface IStorage
 *
 * @package App\Domain\Oga
 */
interface IStorage {
  /**
   * @param DateTimeImmutable $dateTime
   * @param int|null $days
   */
  public function add(DateTimeImmutable $dateTime, ?int $days): void;

  /**
   *
   * @return DateTimeImmutable|null
   */
  public function getLastDateTime(): ?DateTimeImmutable;

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return DateTimeImmutable[]]
   */
  public function getDateTimesLaterThan(DateTimeImmutable $dateTime): array;

  /**
   * @param DateTimeImmutable $dateTime
   *
   * @return void
   */
  public function deleteDateTimesLaterThan(DateTimeImmutable $dateTime): void;

  /**
   * @param int $num
   *
   * @return OgaItem[]
   */
  public function getLastItemsAsLocal(int $num = 7): array;

  /**
   *
   * @return LabelValueItem[]
   */
  public function getAvrDaysByYears(): array;
}
