<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2021
 * Time: 13:11
 */


namespace App\Domain\Oga;


use App\Domain\Common\DateItem as CommonDateItem;
use DateTimeImmutable;

/**
 * Class DateItem
 *
 * @package App\Domain\Oga\UI
 */
class DateItem extends CommonDateItem {

  private ?int $days = null;

  /**
   * DateItem constructor.
   *
   * @param DateTimeImmutable $date
   * @param DateTimeImmutable $today
   * @param int|null $days
   */
  public function __construct(DateTimeImmutable $date, DateTimeImmutable $today, ?int $days) {
    parent::__construct($date, $today);

    if ($days) {
      $this->setDays($days);
    }
  }

  /**
   * @return int|null
   */
  public function days(): ?int {
    return $this->days;
  }

  /**
   * @param int $days
   *
   * @return $this
   */
  public function setDays(int $days): self {
    $this->days = $days;

    return $this;
  }
}
