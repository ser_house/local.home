<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.06.2021
 * Time: 8:42
 */


namespace App\Domain\Oga;


use App\Domain\Common\CalendarBuilder;
use DateInterval;
use DateTimeImmutable;

/**
 * Class HistoryBuilder
 *
 * @package App\Domain\Oga
 */
class HistoryBuilder {
  private IStorage $storage;
  private CalendarBuilder $calendarBuilder;

  /**
   * HistoryBuilder constructor.
   *
   * @param IStorage $storage
   * @param CalendarBuilder $calendarBuilder
   */
  public function __construct(IStorage $storage, CalendarBuilder $calendarBuilder) {
    $this->storage = $storage;
    $this->calendarBuilder = $calendarBuilder;
  }

  /**
   * @param DateTimeImmutable $today
   *
   * @return DateItem[]
   */
  public function buildLastWeekDays(DateTimeImmutable $today): array {
    $last_items = $this->storage->getLastItemsAsLocal();
    $dayBuilder = new DayBuilder($today, $last_items);
    // 6 + сегодня
    $interval = new DateInterval('P6D');
    $firstDate = $today->sub($interval);

    return $this->calendarBuilder->buildWeeks($firstDate, $dayBuilder, 1);
  }
}
