<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 10:14
 */


namespace App\Domain\Ambient\MissingMinutes\UI;


use App\Domain\Ambient\MissingMinutes\DateItem;
use App\Domain\Ambient\MissingMinutes\LogItem;
use App\Domain\Common\UI\Presenter as CommonPresenter;

/**
 * Class Presenter
 *
 * @package App\Domain\Ambient\MissingMinutes\UI
 */
class Presenter extends CommonPresenter {

  /**
   * @param DateItem ...$items
   *
   * @return DateView[]
   */
  public function presentMissingMinutesItems(DateItem ...$items): array {
    $views = [];
    foreach ($items as $item) {
      $views[] = $this->presentMissingMinutesItem($item);
    }

    return $views;
  }

  /**
   * @param DateItem $dateItem
   *
   * @return DateView
   */
  public function presentMissingMinutesItem(DateItem $dateItem): DateView {
    $commonView = $this->presentDateItem($dateItem);
    $view = DateView::buildFromCommon($commonView);

    $view->count = $dateItem->value();

    return $view;
  }

  /**
   * @param LogItem $logItem
   *
   * @return string
   */
  public function presentLogItem(LogItem $logItem): string {
    return $logItem->localDateTime()->format('H:i:s') . ' ' . $logItem->type() . ' ' . $logItem->msg();
  }

  /**
   * @param LogItem ...$items
   *
   * @return string[]
   */
  public function presentLogItems(LogItem ...$items): array {
    $result = [];
    foreach($items as $logItem) {
      $result[] = $this->presentLogItem($logItem);
    }
    return $result;
  }
}
