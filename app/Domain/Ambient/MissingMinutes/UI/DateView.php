<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 26.06.2021
 * Time: 20:22
 */


namespace App\Domain\Ambient\MissingMinutes\UI;


use App\Domain\Common\UI\DateView as CommonDayView;

/**
 * Class DateView
 *
 * @package App\Domain\Ambient\MissingMinutes\UI
 */
class DateView extends CommonDayView {
  public string $count;

  /**
   * @param CommonDayView $commonDayView
   *
   * @return static
   */
  public static function buildFromCommon(CommonDayView $commonDayView): self {
    $view = new self();
    $view->date = $commonDayView->date;
    $view->weekday = $commonDayView->weekday;
    $view->status = $commonDayView->status;
    $view->css_class = $commonDayView->css_class;

    return $view;
  }
}
