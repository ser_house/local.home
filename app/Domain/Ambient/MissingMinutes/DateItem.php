<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.07.2021
 * Time: 10:37
 */

declare(strict_types=1);

namespace App\Domain\Ambient\MissingMinutes;

use App\Domain\Common\DateItem as CommonDateItem;
use DateTimeImmutable;

/**
 * Class DateItem
 *
 * @package App\Domain\Ambient
 */
class DateItem extends CommonDateItem {

  private int $value;

  /**
   * DateItem constructor.
   *
   * @param DateTimeImmutable $date
   * @param DateTimeImmutable $today
   * @param int $value
   */
  public function __construct(DateTimeImmutable $date, DateTimeImmutable $today, int $value) {
    parent::__construct($date, $today);

    $this->value = $value;
  }

  /**
   * @return int
   */
  public function value(): int {
    return $this->value;
  }
}
