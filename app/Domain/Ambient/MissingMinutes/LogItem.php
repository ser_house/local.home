<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.07.2021
 * Time: 23:02
 */


namespace App\Domain\Ambient\MissingMinutes;


use DateTimeImmutable;

class LogItem {
  private DateTimeImmutable $localDateTime;
  private string $type;
  private string $msg;

  /**
   * LogItem constructor.
   *
   * @param DateTimeImmutable $localDateTime
   * @param string $type
   * @param string $msg
   */
  public function __construct(DateTimeImmutable $localDateTime, string $type, string $msg) {
    $this->localDateTime = $localDateTime;
    $this->type = $type;
    $this->msg = $msg;
  }

  /**
   * @return DateTimeImmutable
   */
  public function localDateTime(): DateTimeImmutable {
    return $this->localDateTime;
  }

  /**
   * @return string
   */
  public function type(): string {
    return $this->type;
  }

  /**
   * @return string
   */
  public function msg(): string {
    return $this->msg;
  }
}
