<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.06.2021
 * Time: 18:09
 */


namespace App\Domain\Ambient;

/**
 * Class TemperatureFormatter
 *
 * @package App\Domain\Ambient
 */
class TemperatureFormatter {

  /**
   * @param float $temperature
   *
   * @return string
   */
  public function signed(float $temperature): string {
    $sign = $temperature > 0.0 ? '+' : '';
    return "{$sign}$temperature";
  }
}
