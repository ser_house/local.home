<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 20.06.2021
 * Time: 18:00
 */


namespace App\Domain\Ambient;


use DateTimeImmutable;
use Exception;

interface IAmbientStorage {

  /**
   * @param DateTimeImmutable $systemDateTime
   * @param int $sid
   * @param float $t
   * @param float|null $h
   *
   * @return Measure
   */
  public function add(DateTimeImmutable $systemDateTime, int $sid, float $t, float $h = null): Measure;

  /**
   * @param int $sid
   *
   * @return Measure
   */
  public function getLastBySensorId(int $sid): Measure;

  /**
   * @param int $count
   * @param string $order
   *
   * @return Measure[]
   */
  public function getAvgByHours(int $count, string $order = 'ASC'): array;

  /**
   *
   * @return Measure[]
   * @throws Exception
   */
  public function getAvgByCurrentHour(): array;

  /**
   * @param string $order
   *
   * @return Measure[]
   */
  public function getLastHour(string $order = 'ASC'): array;
}
