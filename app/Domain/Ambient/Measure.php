<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2021
 * Time: 10:28
 */


namespace App\Domain\Ambient;


use DateTimeImmutable;

class Measure {

  private Sensor $sensor;
  private DateTimeImmutable $time;
  private float $t;
  private ?float $h;

  /**
   * Measure constructor.
   *
   * @param Sensor $sensor
   * @param DateTimeImmutable $time
   * @param float $t
   * @param float|null $h
   */
  public function __construct(Sensor $sensor, DateTimeImmutable $time, float $t, ?float $h) {
    $this->sensor = $sensor;
    $this->time = $time;
    $this->t = $t;
    $this->h = $h;
  }

  /**
   * @return Sensor
   */
  public function sensor(): Sensor {
    return $this->sensor;
  }

  /**
   * @return DateTimeImmutable
   */
  public function time(): DateTimeImmutable {
    return $this->time;
  }

  /**
   * @return float
   */
  public function t(): float {
    return $this->t;
  }

  /**
   * @return float|null
   */
  public function h(): ?float {
    return $this->h;
  }
}
