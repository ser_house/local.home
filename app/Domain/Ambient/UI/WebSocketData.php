<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.05.2021
 * Time: 10:12
 */


namespace App\Domain\Ambient\UI;


/**
 * Class WebSocketData
 *
 * @package App\Domain\Ambient\UI
 */
class WebSocketData {
  /** @var MeasureView[] */
  public array $data;

  /**
   * WebSocketData constructor.
   *
   * @param MeasureView ...$measureViews
   */
  public function __construct(MeasureView ...$measureViews) {
    $this->data = $measureViews;
  }

  /**
   *
   * @return array
   */
  public static function doc(): array {
    return [
      'data' => [
        MeasureView::doc()
      ],
    ];
  }
}
