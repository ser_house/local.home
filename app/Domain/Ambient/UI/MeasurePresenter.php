<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2021
 * Time: 10:52
 */


namespace App\Domain\Ambient\UI;


use App\Domain\Ambient\Measure;
use App\Domain\Ambient\TemperatureFormatter;
use App\Domain\Common\UI\DateTimeFormatter\IDateTimeFormatter;

class MeasurePresenter {
  private TemperatureFormatter $temperatureFormatter;

  /**
   * MeasurePresenter constructor.
   *
   * @param TemperatureFormatter $temperatureFormatter
   */
  public function __construct(TemperatureFormatter $temperatureFormatter) {
    $this->temperatureFormatter = $temperatureFormatter;
  }

  /**
   * @param IDateTimeFormatter $timeFormatter
   * @param Measure $measure
   *
   * @return MeasureView
   */
  public function present(IDateTimeFormatter $timeFormatter, Measure $measure): MeasureView {
    $dateTime = $measure->time();
    $raw_time = $dateTime->format('d.m.Y H:i:s');
    $time = new Time($raw_time, $timeFormatter->format($dateTime));

    return new MeasureView(
      $measure->sensor()->key(),
      $time,
      $this->temperatureFormatter->signed($measure->t()),
      (string)$measure->h()
    );
  }

  /**
   * @param IDateTimeFormatter $timeFormatter
   * @param Measure ...$measures
   *
   * @return array
   */
  public function presentViews(IDateTimeFormatter $timeFormatter, Measure ...$measures): array {
    $views = [];
    foreach($measures as $measure) {
      $views[] = $this->present($timeFormatter, $measure);
    }
    return $views;
  }
}
