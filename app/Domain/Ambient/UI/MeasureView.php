<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2021
 * Time: 10:50
 */


namespace App\Domain\Ambient\UI;


class MeasureView {
  public int $sid;
  public Time $time;
  public string $t;
  public string $h;

  /**
   * MeasureView constructor.
   *
   * @param int $sid
   * @param Time $time
   * @param string $t
   * @param string $h
   */
  public function __construct(int $sid, Time $time, string $t, string $h) {
    $this->sid = $sid;
    $this->time = $time;
    $this->t = $t;
    $this->h = $h;
  }

  /**
   *
   * @return array
   */
  public static function doc(): array {
    return [
      'sid' => '',
      'time' => [
        'raw' => '',
        'ui' => '',
      ],
      't' => '',
      'h' => '',
    ];
  }
}
