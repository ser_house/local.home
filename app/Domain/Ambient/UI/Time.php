<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.05.2021
 * Time: 9:57
 */


namespace App\Domain\Ambient\UI;

/**
 * Class Time
 *
 * @package App\Domain\Ambient\UI
 */
class Time {
  public string $raw;
  public string $ui;

  /**
   * Time constructor.
   *
   * @param string $raw
   * @param string $ui
   */
  public function __construct(string $raw, string $ui) {
    $this->raw = $raw;
    $this->ui = $ui;
  }
}
