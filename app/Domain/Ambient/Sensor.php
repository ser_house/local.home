<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.07.2021
 * Time: 7:40
 */


namespace App\Domain\Ambient;


class Sensor {

  public const OUTDOORS1 = 1;

  public const ROOM1 = 2;

  private int $key;

  /**
   * Sensor constructor.
   *
   * @param int $key
   */
  public function __construct(int $key) {
    $this->key = $key;
  }

  /**
   * @return int
   */
  public function key(): int {
    return $this->key;
  }
}
