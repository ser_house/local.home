<?php

namespace App\Domain\Ambient;

class TemperatureNormalizer {

  public function normalize(string $t): float {
    $t = str_replace('+-', '-', $t);
    return str_replace(' ', '', $t);
  }
}