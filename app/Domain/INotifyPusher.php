<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.05.2021
 * Time: 21:39
 */


namespace App\Domain;


/**
 * Interface INotifyPusher
 *
 * @package App\Domain
 */
interface INotifyPusher {
  /**
   * @param string $channel
   * @param $data
   */
  public function send(string $channel, $data): void;

  /**
   * @return string
   */
  public function getToken(): string;
}
