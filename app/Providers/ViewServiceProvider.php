<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.03.2018
 * Time: 09:06
 */

namespace App\Providers;

use App\ViewComposers\ActiveLinkComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class ViewServiceProvider extends ServiceProvider {
  public function boot(): void {
    View::composer('main-menu', ActiveLinkComposer::class);
    View::composer('oga.menu', ActiveLinkComposer::class);
  }
}
