<?php

namespace App\Providers;

use App\Domain\DateTimeService;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\Domain\INotifyPusher;
use App\Framework\CentrifugoNotifyPusher;
use App\Domain\Oga\IStorage as IOgaStorage;
use App\Framework\Oga\Storage as OgaStorage;
use App\Domain\Weight\IStorage as IWeightStorage;
use App\Framework\Weight\Storage as WeightStorage;
use App\Domain\Ambient\IAmbientStorage as IAmbientListing;
use App\Framework\Ambient\AmbientStorage as AmbientListing;

class AppServiceProvider extends ServiceProvider {
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
    $this->app
      ->when(DateTimeService::class)
      ->needs('$local_timezone')
      ->give(function () {
        return config('app.local_timezone');
      })
    ;

    $this->app->bind(IAmbientListing::class, AmbientListing::class);
    $this->app->bind(INotifyPusher::class, CentrifugoNotifyPusher::class);
    $this->app->bind(IOgaStorage::class, OgaStorage::class);
    $this->app->bind(IWeightStorage::class, WeightStorage::class);
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
    Blade::directive('spaceless', function () {
      return '<?php ob_start() ?>';
    });

    Blade::directive('endspaceless', function () {
      return "<?php echo preg_replace('/>\\s+</', '><', ob_get_clean()); ?>";
    });

    Blade::directive('nl2br', function ($expression) {
      return "<?php echo str_replace(PHP_EOL, '<br/>', e($expression)); ?>";
    });
  }
}
