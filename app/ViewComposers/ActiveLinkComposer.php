<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 30.03.2018
 * Time: 21:05
 */

namespace App\ViewComposers;

use Illuminate\View\View;

class ActiveLinkComposer {
  /**
   * Bind data to the view.
   *
   * @param View $view
   *
   * @return void
   */
  public function compose(View $view): void {
    $request = request();
    if ($request) {
      $route = $request->route();
      $name = $route->getName();

      $view->with('active', $name);
    }
  }
}
