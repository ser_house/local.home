<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Ulid\Ulid;

class UlidModel extends Model {
  public $timestamps = false;
  public $incrementing = false;
  protected $keyType = 'string';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id',
  ];

  public static function boot() {
    parent::boot();
    self::creating(function ($model) {
      if (!$model->id) {
        $model->id = (string)Ulid::generate(true);
      }
    });
  }
}
