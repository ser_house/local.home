<?php

namespace App\Models;

/**
 * @property string $id
 * @property string $title
 * @property string $description
 * @property integer $summer_periodicity
 * @property integer $winter_periodicity
 * @property string $last_watering_date
 * @property string $next_watering_date
 * @property boolean $active
 * @property string $added_at
 */
class Plant extends UlidModel {
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'plant';
  /**
   * @var array
   */
  protected $fillable = [
    'title',
    'description',
    'summer_periodicity',
    'winter_periodicity',
    'last_watering_date',
    'next_watering_date',
    'active',
    'added_at',
    ];
}
