<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $sensor_id
 * @property float $temperature
 * @property float $humidity
 * @property string $added_at
 */
class Ambient extends Model {
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'ambient';

  /**
   * The "type" of the auto-incrementing ID.
   *
   * @var string
   */
  protected $keyType = 'integer';

  /**
   * @var array
   */
  protected $fillable = ['added_at', 'sensor_id', 'temperature', 'humidity'];

}
