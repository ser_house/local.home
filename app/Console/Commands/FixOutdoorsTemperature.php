<?php

namespace App\Console\Commands;

use App\Domain\Ambient\IAmbientStorage;
use App\Domain\Ambient\TemperatureNormalizer;
use App\Domain\DateTimeService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixOutdoorsTemperature extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'ambient:fix';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(
    private IAmbientStorage $ambientStorage,
    private DateTimeService $dateTimeService,
    private TemperatureNormalizer $temperatureNormalizer,
  ) {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle() {
    $files = array_filter(glob("/vagrant/home_log/ambient-*"), 'is_file');
    if (!empty($files)) {
      $file = last($files);
      $this->info("Последний файл логов:");
      $this->line($file);
//      return 0;
    }
    else {
      $this->warn('Не найдены файлы логов.');
      return 0;
    }

    $data_by_time = $this->getOutdoorsTemperaturesFromLogsIndexedByTime($file);

//    $bar = $this->output->createProgressBar(count($data_by_time));
//    $bar->start();

    foreach($data_by_time as $item) {
      $systemDateTime = $this->dateTimeService->getSystemDateTimeFromSystemStr($item->system_time);

      $db_time = $systemDateTime->format('Y-m-d H:i:s');
      $dbData = DB::select("SELECT id, temperature FROM ambient WHERE sensor_id = 1 AND TO_CHAR(added_at, 'YYYY-MM-DD HH24:MI:SS') = '$db_time'");

      if (empty($dbData)) {
        $this->ambientStorage->add(
          $systemDateTime,
          (int)$item->indoors->sid,
          (float)$item->indoors->t,
          (float)$item->indoors->h
        );

        $out_t = $this->temperatureNormalizer->normalize($item->outdoors->t);
        $this->ambientStorage->add($systemDateTime, (int)$item->outdoors->sid, $out_t);

        $time = $systemDateTime->format('d.m.Y H:i:s');
        $this->info("Добавлено $time (out_t: $out_t, in_t: {$item->indoors->t}, in_h: {$item->indoors->h}");
      }

//      $bar->advance();
    }
    $this->info("Завершено.");
//    $bar->finish();

    return 0;
  }

  private function getDataWithZeroTemperature(): array {
    $sql = "SELECT
      id,
      TO_CHAR(added_at, 'DD.MM.YYYY HH24:MI:SS') AS time
    FROM ambient
    WHERE temperature = 0.0
    ORDER BY id";
    return DB::select($sql);
  }

  private function getOutdoorsTemperaturesFromLogsIndexedByTime(string $log_file): array {
    $data_by_time = [];
    $file_lines = file($log_file, FILE_SKIP_EMPTY_LINES|FILE_IGNORE_NEW_LINES);
    foreach($file_lines as $line) {
      $matches = [];
      preg_match('/API:Add request ({.*})/', $line, $matches);
      if (isset($matches[1])) {
        $item = json_decode($matches[1]);
        $data_by_time[$item->system_time] = $item;
//          if (false !== strpos($item->outdoors->t, '+-')) {
//            $data_by_time[$item->system_time] = $this->temperatureNormalizer->normalize($item->outdoors->t);
//          }
      }
    }
    return $data_by_time;
  }
}
