<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.07.2021
 * Time: 23:14
 */


namespace App\Framework\Ambient;


use App\Domain\Ambient\MissingMinutes\LogItem;
use App\Domain\DateTimeService;
use DateTimeImmutable;

class MissingMinutesLog {
  private const DATE_CACHE_KEY_FORMAT = 'Y-m-d';

  private DateTimeService $dateTimeService;
  private array $contents_by_date;

  /**
   * MissingMinutesLog constructor.
   *
   * @param DateTimeService $dateTimeService
   */
  public function __construct(DateTimeService $dateTimeService) {
    $this->dateTimeService = $dateTimeService;
    $this->contents_by_date = [];
  }

  /**
   * @param DateTimeImmutable $localDateTime
   *
   * @return LogItem[]
   * @throws \Exception
   */
  public function getLogsByMinute(DateTimeImmutable $localDateTime): array {
    $content = $this->getContentByLocalDate($localDateTime);

    $logs = [];
    $utcDate = $this->dateTimeService->localDateTimeToSystem($localDateTime);
    $interval = new \DateInterval("PT1M");
    $startRangeTime = $utcDate->sub($interval);
    $endRangeTime = $utcDate->add($interval);
    $rangeTimes = [
      $startRangeTime->format('H:i:'),
      $utcDate->format('H:i:'),
      $endRangeTime->format('H:i:'),
    ];
    $range_str = implode('|', $rangeTimes);
    $pattern = "/^\[(?<date>.*($range_str).*)\]\s(?<env>\w+)\.(?<type>\w+):(?<message>.*)/m";
//    $pattern = "/^\[(?<date>.*(21:17:|21:18).*)\]\s(?<env>\w+)\.(?<type>\w+):(?<message>.*)/m";
//    $pattern = "/^\[(?<date>.*$time:.*)\]\s(?<env>\w+)\.(?<type>\w+):(?<message>.*(21:17:|21:18).*)/m";
    preg_match_all($pattern, $content, $matches, PREG_SET_ORDER, 0);

    foreach ($matches as $match) {
      $dateTime = $this->dateTimeService->getLocalDateTimeFromSystemStr($match['date']);
      $logs[] = new LogItem($dateTime, $match['type'], trim($match['message']));
    }

    return $logs;
  }

  /**
   * @param DateTimeImmutable $localDateTime
   *
   * @return string
   * @throws \Exception
   */
  private function getContentByLocalDate(DateTimeImmutable $localDateTime): string {
    $env_log_file = env('AMBIENT_LOG');
    if (empty($env_log_file)) {
      throw new \Exception("Env 'AMBIENT_LOG' doesn't exists.");
    }

    $utcDate = $this->dateTimeService->localDateTimeToSystem($localDateTime);
    $key = $utcDate->format(self::DATE_CACHE_KEY_FORMAT);

    if (!isset($this->contents_by_date[$key])) {
      $this->contents_by_date[$key] = '';

      $path_info = pathinfo($env_log_file);
      $log_filename = $path_info['dirname'] . '/' . $path_info['filename'] . '-' . $utcDate->format('Y-m-d') . '.' . $path_info['extension'];

      if (file_exists($log_filename)) {
        $content = file_get_contents($log_filename);
        if (false !== $content) {
          $this->contents_by_date[$key] = $content;
        }
      }
    }

    return $this->contents_by_date[$key];
  }
}
