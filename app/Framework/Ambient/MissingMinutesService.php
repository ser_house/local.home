<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.07.2021
 * Time: 9:10
 */


namespace App\Framework\Ambient;


use App\Domain\Ambient\MissingMinutes\DateItem;
use App\Domain\DateTimeService;
use DateInterval;
use DateTimeImmutable;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class MissingMinutesService
 *
 *
 * @package App\Framework\Ambient
 */
class MissingMinutesService {
  private DateTimeService $dateTimeService;

  /**
   * MissingMinutesService constructor.
   *
   * @param DateTimeService $dateTimeService
   */
  public function __construct(DateTimeService $dateTimeService) {
    $this->dateTimeService = $dateTimeService;
  }

  /**
   * @param DateTimeImmutable $date
   *
   * @return DateTimeImmutable[]
   * @throws Exception
   */
  public function getByLocalDate(DateTimeImmutable $localDate): array {
    $firstDate = $this->getFirstDateAsLocal();

    if ($firstDate && $localDate->format('Y-m-d') === $firstDate->format('Y-m-d')) {
      $start_time = $firstDate->format('Y-m-d H:i:00');
    }
    else {
      $start_time = $localDate->format('Y-m-d 00:00:00');
    }

    $now = $this->dateTimeService->getNowAsLocal();

    if ($now->format('Y-m-d') !== $localDate->format('Y-m-d')) {
      $nextDate = $localDate->add(new DateInterval('P1D'));
      $end_time = $nextDate->format('Y-m-d 00:00:00');
    }
    else {
      $endTime = $now->sub(new DateInterval('PT1M'));
      $end_time = $endTime->format('Y-m-d H:i:s');
    }

    $tz = $this->dateTimeService->getLocalTimezoneName();
    $sql = "SELECT
           TO_CHAR(date_trunc('minute', time_seq), 'yyyy-mm-dd HH24:MI') AS time
    FROM generate_series(
      '$start_time'::timestamp, 
      '$end_time'::timestamp, 
      INTERVAL '1 minute') AS time_seq
    LEFT JOIN ambient 
      ON date_trunc('minute', ambient.added_at AT TIME ZONE 'utc' AT TIME ZONE '$tz') = time_seq
    WHERE ambient.added_at IS NULL
    ORDER BY time_seq";

    $data = DB::select($sql);
    $result = [];
    foreach ($data as $datum) {
      $result[] = $this->dateTimeService->getLocalDateTimeFromLocalStr($datum->time);
    }

    return $result;
  }

  /**
   * @param DateTimeImmutable|null $dateFrom
   *  если null, то будет взято время с точностью до минут самой первой записи
   * @param DateTimeImmutable|null $dateTo
   *  если null, то будет использовано текущее время с точностью до минут
   *
   * @return DateItem[]
   * @throws Exception
   */
  public function getCountByLocalDatesRange(DateTimeImmutable $dateFrom = null, DateTimeImmutable $dateTo = null): array {
    $localTimezone = $this->dateTimeService->getLocalTimezone();

    if (null === $dateFrom) {
      $dateFrom = $this->getFirstDateAsLocal();
    }
    else {
      $dateFrom = $this->dateTimeService->systemDateTimeToLocal($dateFrom);
    }

    if (null === $dateTo) {
      $dateTo = $this->dateTimeService->getNowAsLocal();
      $minuteInterval = new DateInterval('PT1M');
      $dateTo = $dateTo->sub($minuteInterval);
    }
    else {
      $dateTo = $this->dateTimeService->systemDateTimeToLocal($dateTo);
    }

    $start_time = $dateFrom->format('Y-m-d H:i:00');
    $end_time = $dateTo->format('Y-m-d H:i:00');

    $localTzStr = $this->dateTimeService->getLocalTimezoneName();

    $sql = "SELECT
      time_seq::date AS date,
      COUNT(time_seq) AS count_missing_minutes
    FROM generate_series(
      '$start_time'::timestamp, 
      '$end_time'::timestamp, 
      INTERVAL '1 minute') AS time_seq
    LEFT JOIN ambient 
      ON date_trunc('minute', ambient.added_at AT TIME ZONE 'utc' AT TIME ZONE '$localTzStr') = time_seq
    WHERE ambient.added_at IS NULL
    GROUP BY time_seq::date
    ORDER BY time_seq::date";

    $data = DB::select($sql);

    $today = $this->dateTimeService->getNowAsLocal();
    $items = [];
    foreach ($data as $datum) {
      $date = new DateTimeImmutable($datum->date, $localTimezone);
      $items[] = new DateItem($date, $today, $datum->count_missing_minutes);
    }

    return $items;
  }

  /**
   *
   * @return DateItem[]
   * @throws Exception
   */
  public function getCountForAllTime(): array {
    return $this->getCountByLocalDatesRange();
  }

  /**
   *
   * @return int
   * @throws Exception
   */
  public function getCountForToday(): int {
    $timeFrom = $this->dateTimeService->getNowAsLocal();
    $timeFrom = $timeFrom->setTime(0, 0);

    $items = $this->getCountByLocalDatesRange($timeFrom);
    if (empty($items)) {
      return 0;
    }

    return $items[0]->value();
  }

  /**
   *
   * @return DateTimeImmutable|null
   * @throws Exception
   */
  private function getFirstDateAsLocal(): ?DateTimeImmutable {
    $localTzStr = $this->dateTimeService->getLocalTimezoneName();
    $sql = "SELECT 
        added_at AT TIME ZONE 'utc' AT TIME ZONE '$localTzStr' AS added_at
        FROM ambient 
        ORDER BY id 
        LIMIT 1";
    $data = DB::select($sql);
    if (empty($data)) {
      return null;
    }

    return new DateTimeImmutable($data[0]->added_at, $this->dateTimeService->getLocalTimezone());
  }
}
