<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.07.2021
 * Time: 10:34
 */


namespace App\Framework\Ambient;


use App\Domain\Ambient\Measure;
use App\Domain\Ambient\Sensor;
use App\Domain\DateTimeService;
use App\Models\Ambient;
use DateTimeImmutable;
use Exception;

class Converter {
  private DateTimeService $dateTimeService;

  /**
   * Converter constructor.
   *
   * @param DateTimeService $dateTimeService
   */
  public function __construct(DateTimeService $dateTimeService) {
    $this->dateTimeService = $dateTimeService;
  }

  /**
   * @param Ambient $model
   *
   * @return Measure
   * @throws Exception
   */
  public function modelToMeasure(Ambient $model): Measure {
    $addedAt = $this->dateTimeService->systemDateTimeToLocal(new DateTimeImmutable($model->added_at));
    return new Measure(new Sensor($model->sensor_id), $addedAt, $model->temperature, $model->humidity);
  }
}
