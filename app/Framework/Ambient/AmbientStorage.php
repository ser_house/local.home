<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.05.2021
 * Time: 10:00
 */
declare(strict_types=1);

namespace App\Framework\Ambient;

use App\Domain\Ambient\IAmbientStorage;
use App\Domain\Ambient\Measure;
use App\Domain\Ambient\Sensor;
use App\Domain\DateTimeService;
use App\Models\Ambient;
use DateTimeImmutable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

/**
 * Class AmbientStorage
 *
 * @package App\Framework\Ambient
 */
class AmbientStorage implements IAmbientStorage {

  private DateTimeService $dateTimeService;
  private Converter $converter;

  /**
   * AmbientStorage constructor.
   *
   * @param DateTimeService $dateTimeService
   * @param Converter $converter
   */
  public function __construct(DateTimeService $dateTimeService, Converter $converter) {
    $this->dateTimeService = $dateTimeService;
    $this->converter = $converter;
  }


  /**
   * @inheritDoc
   */
  public function add(DateTimeImmutable $systemDateTime, int $sid, float $t, float $h = null): Measure {
    $data = [
      'added_at' => $systemDateTime->format('Y-m-d H:i:s.v'),
      'sensor_id' => $sid,
      'temperature' => $t,
      'humidity' => null,
    ];
    if ($h) {
      $data['humidity'] = $h;
    }
    try {
      $ambient = Ambient::create($data);
    }
    catch (QueryException $e) {
      Log::channel('ambient')->error('AmbientStorage:add', [
        'data' => $data,
        'error' => $e->errorInfo,
      ]);
    }

    return $this->converter->modelToMeasure($ambient);
  }

  /**
   * @inheritDoc
   */
  public function getLastBySensorId(int $sid): Measure {
    $ambient = Ambient::where('sensor_id', $sid)->orderBy('added_at', 'desc')->first();

    return $this->converter->modelToMeasure($ambient);
  }

  /**
   * @inheritDoc
   */
  public function getAvgByHours(int $count, string $order = 'ASC'): array {
    $tz = $this->dateTimeService->getLocalTimezoneName();

    // Выбираем последние $count агреггированных данных и сортируем их как указано.

    // На каждую минуту записи приходится несколько сенсоров,
    // выбрать надо все записи.
    $count *= $this->getSensorsCount();

    $sql = "SELECT * FROM (
    SELECT
      sensor_id,
      TO_CHAR(date_trunc('hour', added_at) AT TIME ZONE 'utc' AT TIME ZONE '$tz', 'dd.mm.yyyy HH24:MI') AS time,
      ROUND(AVG(temperature), 1) AS temperature,
      ROUND(AVG(humidity), 1) AS humidity
    FROM ambient
    GROUP BY date_trunc('hour', added_at), sensor_id
    ORDER BY date_trunc('hour', added_at) DESC
    LIMIT $count
    ) AS data ORDER BY to_timestamp(time, 'dd.mm.yyyy HH24:MI') $order";

    $data = DB::select($sql);

    $measures = [];

    foreach ($data as $datum) {
      $time = new DateTimeImmutable($datum->time);
      $measures[] = new Measure(new Sensor($datum->sensor_id), $time, (float)$datum->temperature, $datum->humidity ? (float)$datum->humidity : null);
    }

    return $measures;
  }

  /**
   * @inheritDoc
   */
  public function getAvgByCurrentHour(): array {
    $time = $this->dateTimeService->getNowAsLocal();
    $last_hour = $time->format('Y-m-d H');

    $tz = $this->dateTimeService->getLocalTimezoneName();

    // Выбираем последние $count агреггированных данных и сортируем их как указано.

    $sql = "SELECT
      sensor_id,
      TO_CHAR(date_trunc('hour', added_at) AT TIME ZONE 'utc' AT TIME ZONE '$tz', 'dd.mm.yyyy HH24:MI') AS time,
      ROUND(AVG(temperature), 1) AS temperature,
      ROUND(AVG(humidity), 1) AS humidity
    FROM ambient
      AND TO_CHAR(date_trunc('hour', added_at) AT TIME ZONE 'utc' AT TIME ZONE '$tz', 'yyyy-mm-dd HH24') = '$last_hour'
    GROUP BY date_trunc('hour', added_at), sensor_id";

    $data = DB::select($sql);

    $measures = [];

    foreach ($data as $datum) {
      $time = new DateTimeImmutable($datum->time);
      $measures[] = new Measure(new Sensor($datum->sensor_id), $time, (float)$datum->temperature, $datum->humidity ? (float)$datum->humidity : null);
    }

    return $measures;
  }

  /**
   * @inheritDoc
   */
  public function getLastHour(string $order = 'ASC'): array {
    $tz = $this->dateTimeService->getLocalTimezoneName();

    $limit = 60 * $this->getSensorsCount();

    $sql = "SELECT * FROM (
    SELECT
      sensor_id,
      TO_CHAR(date_trunc('minute', added_at) AT TIME ZONE 'utc' AT TIME ZONE '$tz', 'HH24:MI') AS time,
      ROUND(temperature, 1) AS temperature,
      ROUND(humidity, 1) AS humidity
    FROM ambient
    ORDER BY date_trunc('hour', added_at) DESC
    LIMIT $limit
    ) AS data ORDER BY time $order";

    $data = DB::select($sql);

    $measures = [];

    foreach ($data as $datum) {
      $time = new DateTimeImmutable($datum->time);
      $measures[] = new Measure(new Sensor($datum->sensor_id), $time, (float)$datum->temperature, $datum->humidity ? (float)$datum->humidity : null);
    }

    return $measures;
  }

  /**
   *
   * @return int
   */
  public function getSensorsCount(): int {
    static $num_sensors = null;
    if (null === $num_sensors) {
      $data = DB::select('SELECT COUNT(DISTINCT sensor_id) AS num_sensors FROM ambient');
      $num_sensors = $data[0]->num_sensors;
    }

    return $num_sensors;
  }
}
