<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2021
 * Time: 13:04
 */


namespace App\Framework\Weight;


use App\Domain\Common\LabelValueItem;
use App\Domain\DateTimeService;
use App\Domain\Weight\DateItem;
use App\Domain\Weight\IStorage;
use DateTimeImmutable;
use Illuminate\Support\Facades\DB;

/**
 * Class Storage
 *
 * @package App\Framework\Weight
 */
class Storage implements IStorage {
  private DateTimeService $dateTimeService;

  /**
   * Storage constructor.
   *
   * @param DateTimeService $dateTimeService
   */
  public function __construct(DateTimeService $dateTimeService) {
    $this->dateTimeService = $dateTimeService;
  }


  /**
   * @inheritDoc
   */
  public function add(DateTimeImmutable $dateTime, float $value): void {
    $formatted = $dateTime->format('Y-m-d');

    DB::insert('INSERT INTO weight (date, value) VALUES (?, ?)', [$formatted, $value]);
  }

  /**
   * @inheritDoc
   */
  public function getLastDateTime(): ?DateTimeImmutable {
    $last = DB::select('SELECT date FROM weight ORDER BY id DESC LIMIT 1');
    if ($last) {
      return new DateTimeImmutable($last[0]->date);
    }

    return null;
  }

  /**
   * @inheritDoc
   */
  public function getLastItems(int $num = 5): array {
    $today = $this->dateTimeService->getNowAsLocal();

    $tzObj = $this->dateTimeService->getLocalTimezone();

    $data = DB::select("SELECT * FROM (SELECT date, value FROM weight ORDER BY date DESC LIMIT $num) AS data ORDER BY date ASC");
    $items = [];
    foreach ($data as $datum) {
      $items[] = new DateItem(new DateTimeImmutable($datum->date, $tzObj), $today, $datum->value);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function getAvrValuesByYears(): array {
    $sql = "SELECT * FROM (
      SELECT
        TO_CHAR(date_trunc('year', date), 'yyyy') AS year,
        ROUND(AVG(value), 1) AS value
      FROM weight
      GROUP BY date_trunc('year', date)
      ORDER BY date_trunc('year', date) DESC
    ) AS data ORDER BY year ASC";
    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $items[] = new LabelValueItem($datum->year, $datum->value);
    }

    return $items;
  }


}
