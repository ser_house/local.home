<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2021
 * Time: 9:33
 */


namespace App\Framework\Weight;


use App\Domain\DateTimeService;
use App\Domain\Weight\IStorage;
use DateTimeImmutable;
use Exception;


/**
 * Class AddService
 *
 * @package App\Framework\Weight
 */
class AddService {
  private DateTimeService $dateTimeService;
  private IStorage $storage;

  /**
   * AddService constructor.
   *
   * @param DateTimeService $dateTimeService
   * @param IStorage $storage
   */
  public function __construct(DateTimeService $dateTimeService, IStorage $storage) {
    $this->dateTimeService = $dateTimeService;
    $this->storage = $storage;
  }


  /**
   * @param string $datetime
   * @param float $value
   *
   * @return DateTimeImmutable
   * @throws Exception
   */
  public function addByLocalString(string $datetime, float $value): DateTimeImmutable {
    $dateTime = new DateTimeImmutable($datetime, $this->dateTimeService->getLocalTimezone());

    $this->storage->add($dateTime, $value);

    return $dateTime;
  }
}
