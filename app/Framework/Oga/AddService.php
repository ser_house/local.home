<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2021
 * Time: 9:33
 */


namespace App\Framework\Oga;


use App\Domain\DateTimeService;
use App\Domain\Oga\IStorage;
use Exception;

/**
 * Class AddService
 *
 * @package App\Framework\Oga
 */
class AddService {

  /**
   * @param DateTimeService $dateTimeService
   * @param IStorage $storage
   */
  public function __construct(private readonly DateTimeService $dateTimeService, private readonly IStorage $storage) {

  }


  /**
   * @param string $datetime
   *
   * @return int|null days
   * @throws Exception
   */
  public function addByLocalString(string $datetime): ?int {
    $dateTime = $this->dateTimeService->getSystemDateTimeFromLocalStr($datetime);
    $lastDateTime = $this->storage->getLastDateTime();
    $days = null;
    if ($lastDateTime) {
      $days = $this->dateTimeService->getDiffDays($dateTime, $lastDateTime);
    }

    $this->storage->add($dateTime, $days);

    return $days;
  }
}
