<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.04.2018
 * Time: 16:49
 */

namespace App\Framework\Oga;

use DateTimeImmutable;

class Day {

  private readonly DateTimeImmutable $date;
  public readonly int $month;
  public readonly int $week;
  public readonly int $week_day;
  public readonly int $day;
  public readonly bool $is_weekend;

  private static ?DateTimeImmutable $today = null;

  public ?string $time = null;

  /**
   * Day constructor.
   *
   * @param DateTimeImmutable $date
   */
  public function __construct(DateTimeImmutable $date) {
    $this->date = $date;
    $this->week = $this->date->format('W');
    $this->month = $this->date->format('n');
    $this->week_day = $this->date->format('N');
    $this->is_weekend = $this->week_day > 5;
    $this->day = $this->date->format('j');

    if (!self::$today) {
      self::$today = new DateTimeImmutable();
    }
  }

  public function setTime(string $time): self {
    $this->time = $time;
    return $this;
  }

  public function getDateString(): string {
    return $this->date->format('Y-m-d');
  }

  public function isPast(): bool {
    return (int)$this->date->format('Ymd') < (int)self::$today->format('Ymd');
  }

  public function isToday(): bool {
    return (int)$this->date->format('Ymd') === (int)self::$today->format('Ymd');
  }

  public function cssClasses(): string {
    $classes = [];

    if ($this->is_weekend) {
      $classes[] = 'weekend';
    }

    if ($this->isPast()) {
      $classes[] = 'past';
    }

    if ($this->isToday()) {
      $classes[] = 'today';
    }

    if ($this->time) {
      $classes[] = 'has-data';
    }

    return !empty($classes) ? (' ' . implode(' ', $classes)) : '';
  }
}
