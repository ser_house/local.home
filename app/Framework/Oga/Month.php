<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.04.2018
 * Time: 21:09
 */

namespace App\Framework\Oga;


class Month {

  private array $weeks = [];
  private string $name;

  /**
   * Month constructor.
   *
   * @param string $name
   */
  public function __construct(string $name) {
    $this->name = $name;
  }


  public function setDay(Day $day): void {
    $week = $day->week;
    $week_day = $day->week_day;

    if (!isset($this->weeks[$week])) {
      $this->weeks[$week] = array_fill(1, 7, null);
    }

    $this->weeks[$week][$week_day] = $day;
  }

  /**
   * @return array
   */
  public function getWeeks(): array {
    return $this->weeks;
  }

  public function getWeekdayNames(): array {
    return [
      'пн',
      'вт',
      'ср',
      'чт',
      'пт',
      'сб',
      'вс',
    ];
  }

  public function getName(): string {
    return $this->name;
  }
}
