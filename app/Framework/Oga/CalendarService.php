<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.04.2018
 * Time: 16:03
 */

namespace App\Framework\Oga;

use App\Domain\DateTimeService;
use DateInterval;
use DatePeriod;
use DateTimeImmutable;
use DB;


/**
 * Class Service
 *
 * @package App\Service
 */
class CalendarService {

  public function __construct(
    private readonly DateTimeService $dateTimeService,
    private readonly TimeFormatter $timeFormatter) {

  }

  /**
   * @param int|null $year
   *
   * @return array
   */
  public function getYearData(int $year = null): array {
    if (null === $year) {
      $year = (int)date('Y');
    }
    $tz = $this->dateTimeService->getLocalTimezoneName();
    $sql = "SELECT
      time AT TIME ZONE 'utc' AT TIME ZONE '$tz' AS time
    FROM oga
    WHERE TO_CHAR(date_trunc('year', time) AT TIME ZONE 'utc' AT TIME ZONE '$tz', 'yyyy') = :year
    ORDER BY time ASC";
    $data = DB::select($sql, ['year' => $year]);
    $items = [];
    foreach ($data as $datum) {
      $time = new DateTimeImmutable($datum->time);
      $date = $time->format('Y-m-d');
      $items[$date] = $time;
    }

    $days = $this->getYearDays($year);

    return $this->buildMonths($days, $items);
  }

  /**
   * @param int $year
   *
   * @return Day[]
   */
  private function getYearDays(int $year = null): array {
    $start_date = new DateTimeImmutable("$year-01-01");
    $end_date = new DateTimeImmutable("$year-12-31");

    $dates = [];

    $dayInterval = new DateInterval('P1D');

    $period = new DatePeriod($start_date, $dayInterval, $end_date->add($dayInterval));
    foreach ($period as $date) {
      $dates[] = new Day($date);
    }

    return $dates;
  }

  /**
   * @param Day[] $days
   * @param DateTimeImmutable[] $items
   *
   * @return array
   */
  private function buildMonths(array $days, array $items): array {
    $months = [];

    $month_names = $this->timeFormatter->months();

    foreach ($days as $day) {
      $month = $day->month;

      if (!isset($months[$month])) {
        $months[$month] = new Month($month_names[$month]);
      }

      $date_str = $day->getDateString();
      if (isset($items[$date_str])) {
        $day->setTime($items[$date_str]->format('H:i'));
      }
      $months[$month]->setDay($day);
    }

    return $months;
  }
}
