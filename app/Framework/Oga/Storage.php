<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2021
 * Time: 13:04
 */


namespace App\Framework\Oga;


use App\Domain\Common\LabelValueItem;
use App\Domain\DateTimeService;
use App\Domain\Oga\IStorage;
use App\Domain\Oga\OgaItem;
use DateTimeImmutable;
use Illuminate\Support\Facades\DB;

/**
 * Class Storage
 *
 * @package App\Framework\Oga
 */
class Storage implements IStorage {
  private DateTimeService $dateTimeService;

  /**
   * Storage constructor.
   *
   * @param DateTimeService $dateTimeService
   */
  public function __construct(DateTimeService $dateTimeService) {
    $this->dateTimeService = $dateTimeService;
  }


  /**
   * @inheritDoc
   */
  public function add(DateTimeImmutable $dateTime, ?int $days): void {
    $formatted = $dateTime->format('Y-m-d H:i');

    DB::insert('INSERT INTO oga (time, days) VALUES (?, ?)', [$formatted, $days]);
  }

  /**
   * @inheritDoc
   */
  public function getLastDateTime(): ?DateTimeImmutable {
    $last = DB::select('SELECT time FROM oga ORDER BY id DESC LIMIT 1');
    if ($last) {
      return new DateTimeImmutable($last[0]->time);
    }

    return null;
  }

  /**
   * @inheritDoc
   */
  public function getDateTimesLaterThan(DateTimeImmutable $dateTime): array {
    $tz = $this->dateTimeService->getLocalTimezoneName();
    $date_str = $dateTime->format('Y-m-d');
    $sql = "SELECT time FROM oga WHERE TO_CHAR(time, 'yyyy-mm-dd') > '$date_str' ORDER BY time ASC";
    $items = DB::select($sql);
    $dates = [];
    foreach ($items as $item) {
      $dates[] = new DateTimeImmutable($item->time);
    }

    return $dates;
  }

  /**
   * @inheritDoc
   */
  public function deleteDateTimesLaterThan(DateTimeImmutable $dateTime): void {
    $date_str = $dateTime->format('Y-m-d');
    $sql = "DELETE FROM oga WHERE TO_CHAR(time, 'yyyy-mm-dd') > '$date_str'";
    DB::delete($sql);
  }

  /**
   * @inheritDoc
   */
  public function getLastItemsAsLocal(int $num = 7): array {
    $tzObj = $this->dateTimeService->getLocalTimezone();
    $tz = $this->dateTimeService->getLocalTimezoneName();

    $data = DB::select("SELECT * FROM (SELECT time AT TIME ZONE 'utc' AT TIME ZONE '$tz' AS time, days FROM oga ORDER BY time DESC LIMIT $num) AS data ORDER BY time ASC");
    $items = [];
    foreach ($data as $datum) {
      $items[] = new OgaItem(
        new DateTimeImmutable($datum->time, $tzObj),
        $datum->days,
      );
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function getAvrDaysByYears(): array {
    $tz = $this->dateTimeService->getLocalTimezoneName();
    $sql = "SELECT * FROM (
      SELECT
        TO_CHAR(date_trunc('year', time) AT TIME ZONE 'utc' AT TIME ZONE '$tz', 'yyyy') AS year,
        ROUND(AVG(days), 1) AS days
      FROM oga
      GROUP BY date_trunc('year', time)
      ORDER BY date_trunc('year', time) DESC
    ) AS data ORDER BY year ASC";
    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $items[] = new LabelValueItem($datum->year, $datum->days);
    }

    return $items;
  }


}
