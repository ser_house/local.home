<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2021
 * Time: 9:33
 */


namespace App\Framework\Oga;


use App\Domain\DateTimeService;
use App\Domain\Oga\IStorage;
use DateTime;
use DateTimeImmutable;
use DB;
use Throwable;


class ImportService {

  /**
   * @param DateTimeService $dateTimeService
   * @param IStorage $storage
   */
  public function __construct(
    private readonly DateTimeService $dateTimeService,
    private readonly IStorage $storage) {
  }

  /**
   * @param string $content
   *
   * @return [$rows, $items]
   * @throws \Exception
   */
  public function parse(string $content): array {

    $rows = explode(PHP_EOL, $content);
    $items = [];
    foreach ($rows as $row) {
      // Дата может быть как в формате 01.01.2024, так и, например, в формате 01.01.24.
      // Если строить объект времени через str_replace("\t", ' ', $row) и потом просто new DateTimeImmutable,
      // то дата в виде 01.01.24 не распознается из строки 01.01.24 00:00.
      // Поэтому распознаем дату отдельно и потом суём в неё время.
      $cells = explode("\t", $row);
      $dateTime = new DateTime(trim($cells[0]), $this->dateTimeService->getLocalTimezone());
      $time = trim($cells[1]);
      [$hours, $minutes] = explode(':', $time);
      $dateTime->setTime($hours, $minutes);//->setTimezone($systemTz);

      $items[] = DateTimeImmutable::createFromMutable($dateTime);
    }
    sort($rows);
    sort($items);
    return [$rows, $items];
  }

  /**
   * @param string $content
   *
   * @return DateTimeImmutable[]
   * @throws Throwable
   */
  public function import(array $dates, array $times): array {
    $systemTz = $this->dateTimeService->getSystemTimezone();
    $items = [];
    foreach ($dates as $i => $date) {
      $dateTime = new DateTime(trim($date) . ' ' . trim($times[$i]), $this->dateTimeService->getLocalTimezone());
      $dateTime->setTimezone($systemTz);
      $items[] = DateTimeImmutable::createFromMutable($dateTime);
    }

    $firstDateTime = $items[0];

    $db_later_times = $this->storage->getDateTimesLaterThan($firstDateTime);

    DB::beginTransaction();

    try {
      $this->storage->deleteDateTimesLaterThan($firstDateTime);
      $lastDateTime = $this->storage->getLastDateTime();
      $items = array_merge($items, $db_later_times);
      sort($items);

      foreach ($items as $item) {
        $days = null;
        if ($lastDateTime) {
          $days = $this->dateTimeService->getDiffDays($item, $lastDateTime);
        }
        $lastDateTime = $item;
        $this->storage->add($item, $days);
      }
      DB::commit();
    }
    catch (Throwable $e) {
      DB::rollBack();
      throw $e;
    }

    return $items;
  }
}
