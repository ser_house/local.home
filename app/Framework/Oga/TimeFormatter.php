<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.06.2021
 * Time: 7:52
 */


namespace App\Framework\Oga;


use DateTimeImmutable;

/**
 * Class TimeFormatter
 *
 * @package App\Framework\Oga
 */
class TimeFormatter {

  /**
   * @param DateTimeImmutable $dateTime
   * @param int $roundStepMinutes
   *
   * @return string
   */
  public function roundedHoursMinutes(DateTimeImmutable $dateTime, int $roundStepMinutes = 5): string {
    $timestamp = $dateTime->getTimestamp();
    $roundedTimestamp = round($timestamp / ($roundStepMinutes * 60)) * ($roundStepMinutes * 60);
    $newDateTime = $dateTime->setTimestamp($roundedTimestamp);

    return $newDateTime->format('H:i');
  }

  /**
   *
   * @return string[]
   */
  public function months(): array {
    return [
      1 => 'Январь',
      2 => 'Февраль',
      3 => 'Март',
      4 => 'Апрель',
      5 => 'Май',
      6 => 'Июнь',
      7 => 'Июль',
      8 => 'Август',
      9 => 'Сентябрь',
      10 => 'Октябрь',
      11 => 'Ноябрь',
      12 => 'Декабрь',
    ];
  }
}
