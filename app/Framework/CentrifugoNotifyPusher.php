<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 31.05.2021
 * Time: 21:40
 */


namespace App\Framework;


use App\Domain\INotifyPusher;
use phpcent\Client as CentrifugoClient;
use Illuminate\Support\Facades\App;

/**
 * Class CentrifugoNotifyPusher
 *
 * @package App\Framework
 */
class CentrifugoNotifyPusher implements INotifyPusher {
  private CentrifugoClient $client;

  /**
   * CentrifugoNotifyPusher constructor.
   */
  public function __construct() {
    $ws_port = config('app.ws_port');
    $key = config('app.ws_key');
    $secret = config('app.ws_secret');
    $this->client = new CentrifugoClient("http://localhost:$ws_port/api", $key, $secret);
  }

  public function getToken(): string {
    return $this->client->generateConnectionToken(1);
  }

  /**
   * @inheritDoc
   */
  public function send(string $channel, $data): void {
    if (is_object($data)) {
      $data = json_decode(json_encode($data));
    }

    $env = App::environment();
    $this->client->publish("$channel-$env", $data);
  }
}
