<?php

use App\Http\Controllers\Web\Ambient;
use App\Http\Controllers\Web\Plants;
use App\Http\Controllers\Web\Oga;
use App\Http\Controllers\Web\Weight;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Oga\Home::class)->name('main');

Route::prefix('ambient')->group(function () {
  Route::get('/missing-minutes-report', Ambient\MissingMinutesReport::class)->name('ambient.missing_minutes_report');
});

Route::prefix('plants')->group(function () {
  Route::get('/', Plants\Home::class)->name('plants');
  Route::get('new', Plants\NewPlant::class)->name('plants.new');
  Route::post('add', Plants\Add::class)->name('plants.add');
});

Route::prefix('oga')->group(function () {
  Route::get('/', Oga\Home::class)->name('oga');
  Route::post('add', Oga\Add::class)->name('oga.add');
  Route::get('calendar', Oga\Calendar::class)->name('oga.calendar');
  Route::match(['get', 'post'], 'import', Oga\Import::class)->name('oga.import');
});

Route::prefix('weight')->group(function () {
  Route::get('/', Weight\Home::class)->name('weight');
  Route::post('add', Weight\Add::class)->name('weight.add');
});
