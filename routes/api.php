<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Ambient;
use App\Http\Controllers\Api\Plants;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('t')->group(function() {
    Route::post('/', Ambient\Add::class)->name('api.ambient.add');
});

Route::prefix('ambient')->group(function() {
  Route::get('/date_missing_minutes/{date}', Ambient\MissingMinutes\GetDateMinutes::class)->name('api.ambient.get_date_missing_minutes');
});

Route::prefix('plants')->group(function() {
  Route::post('/{id}/watered/{date}', Plants\Watered::class)->name('api.plants.watered');
});
