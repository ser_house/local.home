<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class Ambient extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('ambient', function (Blueprint $table) {
      $table->id();
      $table->unsignedSmallInteger('sensor_id');
      $table->decimal('temperature', 3, 1)->nullable(false);
      $table->decimal('humidity', 3, 1)->nullable()->default(null);
    });

    DB::statement("ALTER TABLE ambient ADD COLUMN added_at TIMESTAMP(3) NOT NULL DEFAULT now()");
    DB::statement("CREATE INDEX by_hour_idx ON ambient (date_trunc('hour', added_at))");
    DB::statement("CREATE INDEX by_date_idx ON ambient ((added_at::date))");
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('ambient');
  }
}
