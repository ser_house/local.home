<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class WegaToWeight extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::rename('wega', 'weight');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {

  }
}
