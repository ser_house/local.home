<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Plant extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('plant', function (Blueprint $table) {
      $table->char('id', 26)->primary();
      $table->string('title');
      $table->string('description')->nullable();
      $table->smallInteger('summer_periodicity', false, true);
      $table->smallInteger('winter_periodicity', false, true);
      $table->date('last_watering_date')->nullable()->default(null);
      $table->date('next_watering_date')->nullable()->default(null);
      $table->boolean('active')->default(true);
    });

    DB::statement("ALTER TABLE plant ADD COLUMN added_at TIMESTAMP(3) NOT NULL DEFAULT now()");
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('plant');
  }
}
