<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WeightSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('weight')->insert([
      [
        'date' => '2001.01.01',
        'value' => 90.5,
      ],
      [
        'date' => '2001.02.01',
        'value' => 91.5,
      ],
    ]);
  }
}
