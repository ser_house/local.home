<?php

namespace Database\Seeders;

use App\Domain\Ambient\Sensor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AmbientSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    DB::table('ambient')->insert([
      [
        'sensor_id' => Sensor::OUTDOORS1,
        'temperature' => 12.5,
        'humidity' => null,
      ],
      [
        'sensor_id' => Sensor::ROOM1,
        'temperature' => 24.4,
        'humidity' => null,
      ],
    ]);
  }
}
