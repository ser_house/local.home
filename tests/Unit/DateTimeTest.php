<?php

namespace Tests\Unit;

use App\Domain\DateTimeService;
use DateTimeImmutable;
use Tests\TestCase;


class DateTimeTest extends TestCase {
  private DateTimeService $dateTimeService;

  protected function setUp(): void {
    parent::setUp();
    $this->dateTimeService = app()->make(DateTimeService::class);
  }

  public function testDiff() {
    $dateTime = $this->dateTimeService->getSystemDateTimeFromLocalStr('2023-01-02 06:00:00');
    $lastDateTime = new DateTimeImmutable('2023-01-01 09:00:00');

    $days = $this->dateTimeService->getDiffDays($dateTime, $lastDateTime);
    self::assertEquals(1, $days);
  }
}
