<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 21.06.2021
 * Time: 7:55
 */

namespace Tests\Unit\Oga;

use App\Framework\Oga\TimeFormatter;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class TimeFormatterTest extends TestCase {

  private TimeFormatter $timeFormatter;

  protected function setUp(): void {
    parent::setUp();
    $this->timeFormatter = app()->make(TimeFormatter::class);
  }


  public function testRoundedHoursMinutes() {
    $dateTime = new DateTimeImmutable('15:43');

    $time = $this->timeFormatter->roundedHoursMinutes($dateTime);
    self::assertEquals('15:45', $time);

    $dateTime = $dateTime->setTime(15, 40);
    $time = $this->timeFormatter->roundedHoursMinutes($dateTime);
    self::assertEquals('15:40', $time);

    $dateTime = $dateTime->setTime(15, 47);
    $time = $this->timeFormatter->roundedHoursMinutes($dateTime);
    self::assertEquals('15:45', $time);

    $dateTime = $dateTime->setTime(15, 48);
    $time = $this->timeFormatter->roundedHoursMinutes($dateTime);
    self::assertEquals('15:50', $time);

    $dateTime = $dateTime->setTime(15, 58);
    $time = $this->timeFormatter->roundedHoursMinutes($dateTime);
    self::assertEquals('16:00', $time);

    $dateTime = $dateTime->setTime(16, 0);
    $time = $this->timeFormatter->roundedHoursMinutes($dateTime);
    self::assertEquals('16:00', $time);

    $dateTime = $dateTime->setTime(16, 1);
    $time = $this->timeFormatter->roundedHoursMinutes($dateTime);
    self::assertEquals('16:00', $time);

    $dateTime = $dateTime->setTime(16, 6);
    $time = $this->timeFormatter->roundedHoursMinutes($dateTime);
    self::assertEquals('16:05', $time);
  }
}
