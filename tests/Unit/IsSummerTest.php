<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.05.2021
 * Time: 12:00
 */

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use DateTimeImmutable;
use DateTimeZone;

class IsSummerTest extends TestCase {

  public function testIsSummerFirstDay() {
    $tz = new DateTimeZone('Europe/Moscow');
    $today = new DateTimeImmutable('2021-03-15', $tz);

    $startSummerDate = new DateTimeImmutable('March 15 2021', $tz);
    $startWinterDate = new DateTimeImmutable('October 15 2021', $tz);

    $todayIsSummer = $today >= $startSummerDate && $today < $startWinterDate;

    self::assertTrue($todayIsSummer);
  }

  public function testIsSummerLastDay() {
    $tz = new DateTimeZone('Europe/Moscow');
    $today = new DateTimeImmutable('2021-10-14', $tz);

    $startSummerDate = new DateTimeImmutable('March 15 2021', $tz);
    $startWinterDate = new DateTimeImmutable('October 15 2021', $tz);

    $todayIsSummer = $today >= $startSummerDate && $today < $startWinterDate;

    self::assertTrue($todayIsSummer);
  }

  public function testIsSummer() {
    $tz = new DateTimeZone('Europe/Moscow');
    $today = new DateTimeImmutable('2021-06-14', $tz);

    $startSummerDate = new DateTimeImmutable('March 15 2021', $tz);
    $startWinterDate = new DateTimeImmutable('October 15 2021', $tz);

    $todayIsSummer = $today >= $startSummerDate && $today < $startWinterDate;

    self::assertTrue($todayIsSummer);
  }
}
