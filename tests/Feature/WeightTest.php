<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.07.2021
 * Time: 18:28
 */

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Tests\TestCaseDb;

class WeightTest extends TestCaseDb {

  public function testPage() {
    $response = $this->get(route('weight'));

    $response->assertStatus(200);
  }

  public function testAdd() {
    $data = [
      'date' => '1.03.2001',
      'value' => 87.5,
    ];

    $this->followingRedirects();
    $response = $this->post(route('weight.add'), $data);
    $response->assertSee('alert-success');

    $items = DB::select('SELECT * FROM weight ORDER BY date DESC');
    self::assertCount(3, $items);
    self::assertEquals('2001-03-01', $items[0]->date);
    self::assertEquals('87.5', $items[0]->value);
  }
}
