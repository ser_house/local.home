<?php

namespace Tests\Feature;

use Tests\TestCaseDb;

class ExampleTest extends TestCaseDb {
  /**
   * A basic test example.
   *
   * @return void
   */
  public function test_example() {
    $response = $this->get('/');

    $response->assertStatus(200);
  }
}
